﻿
using FFD.OrderService.Core.Entities;
using Microsoft.EntityFrameworkCore;
//using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
//using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace FFD.OrderService.DataAccess.Repositories
{
    /// <summary>
    /// Репозиторий для работы с Entity Framework
    /// </summary>
    /// <typeparam name="T">Тип сущности</typeparam>
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        protected readonly DbContext _context;
        private readonly DbSet<T> _entitySet;

        public EfRepository(Context context)
        {
            _context = context;
            _entitySet = _context.Set<T>();
        }

        public async Task<T> AddAsync(T element)
        {
            T entity = (await _entitySet.AddAsync(element)).Entity; 
            await _context.SaveChangesAsync();

            return entity;
        }
        public async Task AddRangeAsync(T[] elements)
        {
            await _entitySet.AddRangeAsync(elements);
            await _context.SaveChangesAsync();
        }

        public async Task<int> DeleteByIdAsync(Guid id)
        {
            //throw new NotImplementedException();
            var entity = await _entitySet.FirstOrDefaultAsync(e => e.Id == id);
            if (entity == null) return 0;

            return await DeleteAsync(entity);
        }
        public async Task<int> DeleteAsync(T entity)
        {
            _entitySet.Remove(entity);
            int recAff = await _context.SaveChangesAsync();

            return recAff;
        }

        public Task<IEnumerable<T>> GetAllAsync()//Task<IEnumerable<T>>
        {
            return Task.FromResult<IEnumerable<T>>(_entitySet);
        }
        public Task<IQueryable<T>> GetAllAsync(string[] navProps)//Task<IEnumerable<T>>
        {
            IQueryable<T> query = _entitySet;

            foreach (var entity in navProps)
                query = query.Include(entity);

            return Task.FromResult(query);
        }

        public async Task<T> GetByIdAsync(params object[] Ids)
        {
            return await _entitySet.FindAsync([..Ids]);//Ids[0], Ids[1]...
        }

        public async Task<T> GetByIdAsync(Expression<Func<T, bool>> filter, string[] navProps)
        {
            IQueryable<T> query = _entitySet;

            foreach (var entity in navProps)
                query = query.Include(entity);

            return await query.Where(filter).FirstOrDefaultAsync();
        }

        //public IQueryable<T> FindBy(Expression<Func<T, bool>> expression) => DatabaseContext.Set<T>().Where(expression).AsNoTracking();
        public IQueryable<T> FindAllBy(Expression<Func<T, bool>> predicate)
        {
            return _context.Set<T>().Where(predicate).AsNoTracking();
        }
        public Task<List<T>> FindAllByAsync(Expression<Func<T, bool>> predicate)
        {
            return _context.Set<T>().Where(predicate).ToListAsync();
        }


        #region SaveChanges

        /// <summary>
        /// Сохранить изменения.
        /// </summary>
        public int SaveChanges()
        {
            return _context.SaveChanges();
        }

        /// <summary>
        /// Сохранить изменения асинхронно
        /// </summary>
        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await _context.SaveChangesAsync(cancellationToken);
        }

        #endregion

    }
}