﻿using FFD.OrderService.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace FFD.OrderService.DataAccess.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private Dictionary<Type, object> _repositories = [];

        public Context Context { get; init; }        

        public UnitOfWork(Context context)
        {
            Context = context;
            //_repositories = new Dictionary<Type, object>();
        } 

        public IRepository<T> GetRepository<T>() where T : BaseEntity
        {
            if (_repositories.ContainsKey(typeof(T)))
            {
                return (IRepository<T>)_repositories[typeof(T)];
            }

            var repository = new EfRepository<T>(Context);
            _repositories.Add(typeof(T), repository);

            return repository;
        }

        /// <summary>
        /// Сохранить изменения асинхронно
        /// </summary>
        public async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return await Context.SaveChangesAsync(cancellationToken);
        }

        //Dispose pattern
        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    Context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}