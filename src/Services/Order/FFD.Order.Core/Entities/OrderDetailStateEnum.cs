﻿using System.ComponentModel;

namespace FFD.OrderService.Core.Entities
{
    public enum OrderDetailStateEnum //: BaseEntity
    {
        [Description("Позиция отменена")]
        Canceled = 1,
        
        [Description("Позиция не обработана")]
        NotProcessed = 2,

        [Description("Позиция готовится")]
        Cooking = 3,

        [Description("Позиция готова")]
        Ready = 4
    }
}