﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFD.OrderService.Core.Entities
{
    //[Index(nameof(PhoneNumber), IsUnique = true)]
    //[Index(nameof(Email), IsUnique = true)]
    public class Client: BaseEntity
    {
        public required string PhoneNumber {  get; set; }
        public string Email { get; set; }
        public string Name { get; set; }

        public List<Order> Orders { get; } = [];
    }
}