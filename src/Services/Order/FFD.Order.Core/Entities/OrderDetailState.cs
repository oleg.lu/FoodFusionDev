﻿using System.ComponentModel.DataAnnotations;

public class OrderDetailState
{
    [Key]
    public int Id {  get; set; }                                                                    
    public string Name { get; set; }                
    public string Description { get; set; }   
}