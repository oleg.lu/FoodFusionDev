﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFD.OrderService.Core.Entities
{
    public class Dish: BaseEntity
    {
        public string Name {  get; set; }
        public double Price { get; set; }
        public double Weight { get; set; }
        public string? Description { get; set; }
    }
}
