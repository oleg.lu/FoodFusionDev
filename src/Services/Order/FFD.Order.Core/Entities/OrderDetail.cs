﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFD.OrderService.Core.Entities
{
    public class OrderDetail : BaseEntity
    {
        public Order Order { get; set; }// Required reference navigation to principal
        public Guid OrderId { get; set; } // Required foreign key property
        public Guid DishId { get; set; }
        public Dish Dish { get; set; }
        public double Price { get; set; }
        public double Discount { get; set; }
        public uint Count { get; set; }

        public OrderDetailStateEnum State { get; set; }
        public DateTime? LastStateDate { get; set; }

        public double Total {
            get
            {
                return (Price - Discount) * Count;
            }
        }
    }
}
