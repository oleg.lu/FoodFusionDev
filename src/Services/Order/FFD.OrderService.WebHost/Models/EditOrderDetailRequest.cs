﻿using FFD.OrderService.Core.Entities;

namespace FFD.OrderService.WebHost.Models
{
    public class EditOrderDetailRequest
    {
        ///<example>44328385-ddbf-4e78-8b51-c9739e67078d</example>
        public Guid OrderId { get; set; }
        ///<example>67dc8108-427a-4a86-bbfd-22f0a2b1045c</example>
        public Guid DishId { get; set; }

        /// <example>4</example>
        public int State { get; set; }
    }
}