﻿namespace FFD.OrderService.WebHost.Models
{
    public class DeleteOrderDetailRequest
    {
        public Guid OrderId { get; set; } 
        public Guid DishId { get; set; }
    }
}