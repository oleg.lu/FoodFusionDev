﻿namespace FFD.OrderService.WebHost.Models
{
    public class CreateOrderDetailRequest
    {
        public Guid DishId { get; set; }
        public double Price { get; set; }
        public double Discount { get; set; }
        public uint Count { get; set; }
    }
}