﻿namespace FFD.OrderService.WebHost.Models
{
    public class CreateOrEditClientRequest
    {
        public required string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
    }
}
