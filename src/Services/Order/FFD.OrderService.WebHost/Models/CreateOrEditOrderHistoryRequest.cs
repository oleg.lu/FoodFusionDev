﻿using FFD.OrderService.Core.Entities;

namespace FFD.OrderService.WebHost.Models
{
    public class CreateOrEditOrderHistoryRequest
    {
        public Guid OrderId { get; set; } // Required foreign key property
        public DateTime StatusDate { get; set; }
        public OrderState Status { get; set; }
    }
}