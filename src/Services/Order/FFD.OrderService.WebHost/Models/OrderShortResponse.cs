﻿namespace FFD.OrderService.WebHost.Models
{
    public class OrderShortResponse
    {
        public Guid Id { get; set; }
        public string Number { get; set; }
        public DateTime Date { get; set; }
        public double Total {  get; set; }
        public OrderStateEnum State { get; set; }
        public string StateText { get; set; }
    }
}
