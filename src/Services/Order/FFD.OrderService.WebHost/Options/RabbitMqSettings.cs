﻿namespace FFD.OrderService.WebHost.Options
{
    public class RabbitMqSettings
    {
        public string HostName { get; set; }
        public ushort Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string VirtualHost { get; set; }
    }
}
