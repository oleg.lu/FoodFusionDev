﻿using FDD.Bus.Models;
using FFD.OrderService.Core.Entities;
using FFD.OrderService.DataAccess;
using FFD.OrderService.DataAccess.Repositories;
using FFD.OrderService.WebHost.Logic;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System.Text.Json;

namespace FFD.OrderService.WebHost.Consumers
{
    public class CreateOrderConsumer : IConsumer<BusCreateOrder>
    {
        readonly ILogger<CreateOrderConsumer> _logger;
        readonly OrderHelper _orderHelper;

        public CreateOrderConsumer(ILogger<CreateOrderConsumer> logger,
                                   IUnitOfWork unitOfWork)
        {
            _logger = logger;
            _orderHelper = new OrderHelper(unitOfWork);
        }
        public async Task Consume(ConsumeContext<BusCreateOrder> context)
        {
            var msg = context.Message;
            //Console.WriteLine(JsonSerializer.Serialize(msg));
            _logger.LogInformation($"Order submitted from client: {msg.ClientId}, date={msg.RegistrationDate}, MessageId={context.MessageId}");

            //group by DishId
            var itemList = msg.Items
                .GroupBy(i => i.DishId)
                .Select(g => new OrderDetail() {
                                    DishId = g.Key,
                                    Count = (uint)g.Sum(gi => gi.Count),
                                    Price = g.Min(gi => gi.Price)
                                 }
                ).ToArray();

            //create Order        //var total = itemList.Sum(i => i.Total);
            var order = await _orderHelper.CreateAsync(msg.ClientId, itemList);

            _logger.LogInformation($"Order id={order.Id} has been created");
        }
    }
}