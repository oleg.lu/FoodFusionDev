﻿using FDD.Bus.Models;
using FFD.OrderService.DataAccess;
using FFD.OrderService.DataAccess.Repositories;
using FFD.OrderService.WebHost.Consumers;
using FFD.OrderService.WebHost.Notifier;
using FFD.OrderService.WebHost.Options;
using FFD.Shared.RabbitMq.Settings;
using FFD.Shared.RabbitMq;
using MassTransit;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using System.Reflection;
using System.Text.Json.Serialization;
using FDD.Shared.Interfases;
using FDD.Shared.Services;

namespace WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            //Fix As part of addressing dotnet/aspnetcore#4849, ASP.NET Core MVC trims the suffix Async from action names by default.
            services.AddControllers().AddMvcOptions(x =>
                x.SuppressAsyncSuffixInActionNames = false);

            //!!!!!!!!!!!!!!!!!!!!!!!
            //Fixing the error “A possible object cycle was detected” 
            //!!!!!!!!!!!!!
            services.AddControllers().AddJsonOptions(x=>
                            x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);
            services.AddEndpointsApiExplorer();
            //Swashbuckle
            services.AddSwaggerGen(opts =>
            {//add <GenerateDocumentationFile>true</GenerateDocumentationFile> to <project_name>.csproj
                //Include docs from current API assembly
                var execAsm = Assembly.GetExecutingAssembly();
                var xmlFilename = $"{execAsm.GetName().Name}.xml";
                opts.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));

                // Additionally include the documentation of all other "relevant" projects
                var referencedProjectsXmlDocPaths = execAsm.GetReferencedAssemblies()
                    .Where(assembly => assembly.Name != null && assembly.Name.StartsWith("FFD.Shared", StringComparison.InvariantCultureIgnoreCase))
                    .Select(assembly => Path.Combine(AppContext.BaseDirectory, $"{assembly.Name}.xml"))
                    .Where(path => File.Exists(path));
                foreach (var xmlDocPath in referencedProjectsXmlDocPaths)
                {
                    opts.IncludeXmlComments(xmlDocPath);
                }
            });
            //services.AddOpenApiDocument(options =>
            //{
            //    options.Title = "CookOrder";
            //    options.Version = "1.0";
            //});
            
            string strConn = Configuration.GetConnectionString("Remote"); // ["Remote" | "Local"]
            Console.WriteLine($"ORM ConnectionString:{strConn}");

            services.AddDbContext<Context>(
                option => option.UseNpgsql(strConn)
                .LogTo(Console.WriteLine, LogLevel.Information)
                .EnableSensitiveDataLogging()
                );

            services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>)); //???
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddScoped<IUserAccessor, UserAccessor>();

            #region rabbitMQ Masstransit
            //((ConfigurationManager)Configuration).AddJsonFile("rabbitmq.json");

            //var rabbitSettings = Configuration.GetSection("RabbitMQ");

            ////MassTransit
            //var options = rabbitSettings.Get<RabbitMqSettings>();

            //services.AddOptions<RabbitMqTransportOptions>()
            //        .Configure(o =>
            //        {
            //            o.Host = options.HostName;
            //            o.VHost = options.VirtualHost;
            //            o.User = options.UserName;
            //            o.Pass = options.Password;
            //            o.Port = options.Port;
            //         });

            //services.AddMassTransit(cfg =>
            //{
            //    //add consumers
            //    cfg.AddConsumer<CreateOrderConsumer>();
            //    cfg.AddConsumer<CreateOrderFaultConsumer>();
            //    cfg.AddReceiveEndpointObserver<ReceiveEndpointObserver>(); //обработка ошибок брокера

            //    //var assembly = typeof(Program).Assembly;
            //    //cfg.AddConsumers(assembly);

            //    cfg.UsingRabbitMq((context, cfgFact) =>
            //    {
            //        cfgFact.ConfigureEndpoints(context);
            //    });
            //});
            #endregion

            #region RabbitMq configure
            services.Configure<SenderSettings>(Configuration.GetSection("RabbitMq").GetSection("Sender"));
            services.Configure<BrokerSettings>(Configuration.GetSection("RabbitMq").GetSection("Broker"));
            services.AddScoped<QueueSender>(provider =>
            {
                var senderSettings = provider.GetRequiredService<IOptions<SenderSettings>>().Value;
                var brokerSettings = provider.GetRequiredService<IOptions<BrokerSettings>>().Value;
                return new QueueSender(senderSettings, brokerSettings);
            });

            services.AddScoped<OrderNotifier>();
            services.AddHttpContextAccessor();
            #endregion

            //CORS
            services.AddCors();
        }

        public void Configure(WebApplication app, IWebHostEnvironment env)
        {
            if (!env.IsDevelopment())
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }
            else
            {
                #region Swashbuckle
                app.UseSwagger();
                app.UseSwaggerUI();
                #endregion

                //app.UseOpenApi();
                //app.UseSwaggerUi3(x =>
                //{
                //    x.DocExpansion = "list";
                    
                //});

                //recreate DB
                using (var scope = app.Services.CreateScope())
                {
                    var dbContext = scope.ServiceProvider.GetRequiredService<Context>();

                    dbContext.Database.EnsureDeleted();
                    dbContext.Database.EnsureCreated();
                }
            }
            //CORS
            //app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.UseCors(builder =>
            {
                builder.WithOrigins(app.Configuration.GetSection("CORS:Origins").Get<string[]>())
                       .WithHeaders(app.Configuration.GetSection("CORS:Headers").Get<string[]>())
                       .WithMethods(app.Configuration.GetSection("CORS:Methods").Get<string[]>());
            });

            app.UseHttpsRedirection();
            //app.UseStaticFiles();
            //app.UseRouting();
            app.UseAuthorization();
            app.MapControllers();
            
        }
    }
}