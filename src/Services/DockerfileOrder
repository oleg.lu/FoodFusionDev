#See https://aka.ms/customizecontainer to learn how to customize your debug container and how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:8.0 AS base
USER app
WORKDIR /app
EXPOSE 8080
EXPOSE 8081

FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
ARG BUILD_CONFIGURATION=Release
WORKDIR /src
COPY ["Shared/FFD.Shared.csproj", "Shared/"]
COPY ["Order/FFD.OrderService.WebHost/FFD.OrderService.WebHost.csproj", "FFD.OrderService.WebHost/"]
COPY ["Order/FFD.Order.DataAccess/FFD.OrderService.DataAccess.csproj", "FFD.Order.DataAccess/"]
COPY ["Order/FFD.Order.Core/FFD.OrderService.Core.csproj", "FFD.Order.Core/"]
RUN dotnet restore "./FFD.OrderService.WebHost/./FFD.OrderService.WebHost.csproj"
COPY . .
WORKDIR "/src/Order/FFD.OrderService.WebHost"
RUN dotnet build "./FFD.OrderService.WebHost.csproj" -c $BUILD_CONFIGURATION -o /app/build

FROM build AS publish
ARG BUILD_CONFIGURATION=Release
RUN dotnet publish "./FFD.OrderService.WebHost.csproj" -c $BUILD_CONFIGURATION -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "FFD.OrderService.WebHost.dll"]