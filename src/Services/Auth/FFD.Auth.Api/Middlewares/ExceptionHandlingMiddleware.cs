﻿using FluentValidation;
using System.Net;

namespace FFD.Auth.Api.Middlewares
{
    public class ExceptionHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly bool _showStackTrace;
        private readonly ILogger<ExceptionHandlingMiddleware> _logger;
        private const string requestIdHeaderName = "app-request-id";

        public ExceptionHandlingMiddleware(RequestDelegate next, bool showStackTrace, ILogger<ExceptionHandlingMiddleware> logger)
        {
            _next = next;
            _showStackTrace = showStackTrace;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                context.Request.Headers.Add(requestIdHeaderName, Guid.NewGuid().ToString());

                await this._next(context);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                await HandleExceptionAsync(context, ex);
            }
        }

        private Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var result = "";

            var requestId = context.Request.Headers[requestIdHeaderName];

            switch (exception)
            {
                //case NotFoundException notFoundException:
                //    context.Response.ContentType = "text/plain";
                //    context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                //    result = notFoundException.Message;
                //    break;
                //case NotAuthorizedException accessException:
                //    context.Response.ContentType = "text/plain";
                //    context.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                //    result = accessException.Message;
                //    break;
                //case ValidationException:
                //    context.Response.ContentType = "text/plain";
                //    context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                //    result = exception.Message;
                //    break;
                default:
                    context.Response.ContentType = "text/plain";
                    context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                    result = !_showStackTrace ? $"Sorry, something went wrong. Send this id to admin: {requestId}" :
                        $"{exception.GetType()}: {exception.Message}\n{exception.StackTrace}" ?? "Sorry, no stack trace.";
                    break;
            }

            if (!_showStackTrace && context.Response.StatusCode == 500)
            {
                _logger.LogError(exception, $"Request Id: {requestId}");
            }

            return context.Response.WriteAsync(result);
        }
    }
}