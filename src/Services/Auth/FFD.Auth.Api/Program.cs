﻿using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using NLog.Web;
using System.Text.Json.Serialization;
using System.Text;
using NLog;
using System.Reflection;
using FFD.Auth.Infrastructure;
using Microsoft.EntityFrameworkCore;
using FFD.Auth.Core.Settings;
using FFD.Auth.Application.Behaviors;
using FFD.Auth.Api;
using FFD.Auth.Api.Middlewares;
using FFD.Auth.Application;
using FFD.Shared.Models.Settings;
using FFD.Shared.Extensions;

var logger = NLog.LogManager.Setup().LoadConfigurationFromAppSettings().GetCurrentClassLogger();

try
{
    var builder = WebApplication.CreateBuilder(args);    
    builder.Services.AddDbContext<ApplicationDbContext>(options =>
                                                        options.UseNpgsql(builder.Configuration.GetConnectionString("AuthDatabaseConnection")));

    //Получаем настройки из секции  Authentication в файле appsettings.json
    var authenticationSection = builder.Configuration.GetSection("Authentication");
    var authOptions = authenticationSection.Get<AuthOptions>();
    builder.Services.Configure<AuthOptions>(authenticationSection);

    //Добавляем аутентификацию и авторизацию на основе jwt-токенов 
    //builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    //    .AddJwtBearer(options =>
    //    {
    //        options.RequireHttpsMetadata = true;
    //        options.SaveToken = true;
    //        options.TokenValidationParameters = new TokenValidationParameters
    //        {
    //            ValidateIssuer = true,
    //            ValidIssuer = authOptions.JwtToken.Issuer,
    //            ValidateAudience = true,
    //            ValidAudience = authOptions.JwtToken.Audience,
    //            ValidateLifetime = true,
    //            IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(authOptions.JwtToken.SecretKey)),
    //            ValidateIssuerSigningKey = true
    //        };
    //    });
    var jwtOptions = JwtService.GetJwtBearerOptions(authOptions);
    builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.RequireHttpsMetadata = jwtOptions.RequireHttpsMetadata;
        options.SaveToken = jwtOptions.SaveToken;
        options.TokenValidationParameters = jwtOptions.TokenValidationParameters;
    });
    builder.Services.AddControllers().AddJsonOptions(options =>
    {
        //options.JsonSerializerOptions.PropertyNamingPolicy = null;
        options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
        //options.JsonSerializerOptions.ReferenceHandler = System.Text.Json.Serialization.ReferenceHandler.Preserve;
    });

    //Подключаем MediatR
    builder.Services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblies(Assembly.GetExecutingAssembly()));
    builder.Services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(typeof(Application).Assembly));
    // NLog: Setup NLog for Dependency injection
    builder.Logging.ClearProviders();
    builder.Host.UseNLog();

    //Подключаем AutoMapper
    builder.Services.AddAutoMapper(typeof(Application));

    //Подключаем Fluent Validation
    //builder.Services.AddValidatorsFromAssemblyContaining<Application>();
    //Подключаем валидацию запросов и команд с помощью Fluent Validation
    builder.Services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));


    //Подключаем Swagger
    builder.Services.AddEndpointsApiExplorer();
    //Подключаем в Swagger воозможность добавить токен в заголовки запроса
    builder.Services.AddSwaggerGen(c =>
    {
        c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
        {
            In = ParameterLocation.Header,
            Description = "Please insert JWT with Bearer into field",
            Name = "Authorization",
            Type = SecuritySchemeType.ApiKey
        });

        c.AddSecurityRequirement(new OpenApiSecurityRequirement
        {
            {
                new OpenApiSecurityScheme
                {
                   Reference = new OpenApiReference
                   {
                     Type = ReferenceType.SecurityScheme,
                     Id = "Bearer"
                   }
                },
                new string[] { }
            }
        });
    });

    //Ресолвим зависимость, чтобы получать контекст текущего пользователя в запросах и командах
    builder.Services.AddHttpContextAccessor();

    //Разрешаем зависимости
    builder.InjectDependencies();


    var app = builder.Build();

    var isDevelopment = app.Environment.IsDevelopment();

    app.UseMiddleware<ExceptionHandlingMiddleware>(isDevelopment);

    // Configure the HTTP request pipeline.
    if (isDevelopment)
    {
        app.UseSwagger();
        app.UseSwaggerUI();
    }
    app.UseHttpsRedirection();
    app.UseAuthentication();
    app.UseAuthorization();

    app.MapControllers();

    app.Run();
}
catch (Exception exception)
{
    // NLog: catch setup errors
    logger.Error(exception, "Stopped program because of exception");
    throw;
}
finally
{
    // Ensure to flush and stop internal timers/threads before application-exit (Avoid segmentation fault on Linux)
    NLog.LogManager.Shutdown();
}