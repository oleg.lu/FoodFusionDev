﻿using FFD.Auth.Application.CQRS.User.Commands;
using FFD.Auth.Application.CQRS.User.Queries;
using FFD.Auth.Core;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FFD.Auth.Api.Controllers
{
    [ApiController]
    //[Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly IMediator _mediator;
        public UserController(ILogger<UserController> logger, IMediator mediator)
        {
            _logger = logger;
            _mediator = mediator;
        }
        [HttpPost("CreateUser"), Authorize]
        public async Task<Response<object>> CreateUser(CreateUserCommand request, CancellationToken cancellationToken = default)
        {
            try
            {
                await _mediator.Send(request, cancellationToken);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return Response<object>.Fail(500, "Error create user");
            }
            return Response<object>.Success(true);
        }

        [HttpPost("GetUsers"), Authorize]
        public async Task<Response<object>> GetUsers(GetUsersQuery request, CancellationToken cancellationToken = default)
        {
            var result = await _mediator.Send(request, cancellationToken);
            return Response<object>.Success(result);
        }

        [HttpPost("UpdateUser"), Authorize]
        public async Task<Response<object>> UpdateUser(UpdateUserCommand request, CancellationToken cancellationToken = default)
        {
            try
            {
                await _mediator.Send(request, cancellationToken);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return Response<object>.Fail(500, "Error update user");
            }
            return Response<object>.Success(true);
        }

        [HttpDelete("DeleteUser")]
        public async Task<Response<object>> DeleteUser(DeleteUserCommand request, CancellationToken cancellationToken = default)
        {
            try
            {
                await _mediator.Send(request, cancellationToken);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return Response<object>.Fail(500, "Error delete user");
            }
            return Response<object>.Success(true);
        }

        [HttpPost("Register")]
        public async Task<Response<object>> RegisterUser(RegisterUserCommand request, CancellationToken cancellationToken = default)
        {
            try
            {
                await _mediator.Send(request, cancellationToken);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                return Response<object>.Fail(500, ex.Message);
            }
            return Response<object>.Success(true);
        }
    }
}