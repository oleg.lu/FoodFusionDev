﻿using FFD.Auth.Core.Entities.Db;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Reflection.Metadata;

namespace FFD.Auth.Core
{
    public interface IDbContext
    {
        DbSet<Entities.Db.Setting> Settings { get; set; }
        DbSet<Entities.Db.User> Users { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
        EntityEntry<T> Entry<T>(T entity) where T : class;
    }
}