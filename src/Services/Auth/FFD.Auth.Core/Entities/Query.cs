﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFD.Auth.Core.Query
{
    public class Query
    {
        public Pagination? pagination;
    }
    public class Pagination
    {
        public int Current { get; set; }
        public int PageSize { get; set; }
    }
    public class ResponsibleList<T>
    {
        public required IEnumerable<T> List { get; set; }
        public int? Total { get; set; }
    }
}
