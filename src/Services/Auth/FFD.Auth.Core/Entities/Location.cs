﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FFD.Auth.Core.Entities
{
    public class Location
    {
        public string? Protocol { get; set; }
        public string? Port { get; set; }
        public string? HostName { get; set; }
        public string? PathName { get; set; }
        public Location(string url)
        {
            var arr = url.Split('/');
            Protocol = arr[0].Replace(":", "");
            HostName = arr[2].IndexOf(":") > 0 ? arr[2].Split(':')[0] : arr[2];
            Port = arr[2].IndexOf(":") > 0 ? arr[2].Split(':')[1] : (Protocol == "http" ? "80" : "443");
            string regex = @"^https?://[^/]+([/][^?#]*)?";
            Match match = Regex.Match(url, regex);
            string path = match.Success ? match.Groups[1].Value : "";
            string[] segments = path.Split('/');
            string[] subSegments = segments.Skip(1).Take(segments.Length - 4).ToArray();
            PathName = "/" + string.Join("/", subSegments) + "/";
        }
    }
}
