﻿namespace FFD.Auth.Core.Entities.Dto;
public partial class UserDto
{
    public Guid Id { get; set; }

    public string? FirstName { get; set; }
    public string? MiddleName { get; set; }

    public string? Login { get; set; }

    public string? Email { get; set; }

    public string? Password { get; set; }

    public string? RefreshToken { get; set; }

    public DateTime? TokenExpires { get; set; }

    public string? Avatar { get; set; }

    public byte[]? PasswordHash { get; set; }

    public byte[]? PasswordSalt { get; set; }

    public DateTime? TokenCreated { get; set; }

    public string? LastName { get; set; }

    public string? Status { get; set; }
    public string? Role { get; set; }
    public int? LoginFailures { get; set; }
    public bool? RequiredChangePassword { get; set; }
    public string? Address { get; set; }
}

