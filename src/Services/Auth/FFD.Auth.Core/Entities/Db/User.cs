﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FFD.Auth.Core.Entities.Db
{
    public partial class User
    {
        public Guid Id { get; set; }
        //public string? DisplayName { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? MiddleName { get; set; }
        public string? Email { get; set; }
        public string? Avatar { get; set; }
        public string? Login { get; set; }
        public string? RefreshToken { get; set; }
        public byte[]? PasswordSalt { get; set; }
        public DateTime? TokenExpires { get; set; }
        public DateTime? TokenCreated { get; set; }
        public string? Status { get; set; }
        public byte[]? PasswordHash { get; set; }
        public bool? RequiredChangePassword { get; set; }
        public short? LoginFailures { get; set; }      
        public string? ResetPasswordToken { get; set; }
        public RoleEnum? Role { get; set; }
        public bool? ConfirmEmail { get; set; }
        public string? EmailConfirmationToken { get; set; }
        public string? Address { get; set; }
        


    }
}
