﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFD.Auth.Core.Entities.Db
{
    public partial class Setting
    {
        public int Id { get; set; }

        public string? Key { get; set; }

        public string? Value { get; set; }
    }
}
