﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFD.Auth.Core.User
{
    public class RefreshToken
    {
        public string Token { get; set; } = string.Empty;
        public DateTime Created { get; set; } = DateTime.Now;
        public DateTime Expires { get; set; }
    }
    public class Token
    {
        public string AccessToken { get; set; }
        public string Username { get; set; }
    }
    public class TokenDto
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        public string Login { get; set; }
        public string? Avatar { get; set; }
        public Guid? UserId { get; set; }
        public TokenDto()
        {
            AccessToken = string.Empty;
            RefreshToken = string.Empty;
            Login = string.Empty;
        }
        public TokenDto(Guid userId, string accessToken, string refreshToken, string login)//, string? avatar)
        {
            AccessToken = accessToken;
            RefreshToken = refreshToken;
            Login = login;
            //Avatar = avatar;
            UserId = userId;
        }
    }
    public class Account
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public Account()
        {
            this.Login = string.Empty;
            this.Password = string.Empty;
        }
    }
}
