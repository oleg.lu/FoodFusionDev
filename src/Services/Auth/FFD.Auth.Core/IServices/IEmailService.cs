﻿using FFD.Auth.Core.Entities.Db;
using FFD.Auth.Core.Settings;
using FFD.Auth.Core.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FFD.Auth.Core.IServices
{
    public interface IEmailService
    {
        Task SendEmailAsync(string toEmail, string subject, string message);
        Task SendConfirmationEmailAsync(Guid userId);
    }
}
