﻿namespace FFD.Auth.Application.Exceptions
{
    public class NotAuthorizedException : ApplicationException
    {
        public NotAuthorizedException(string message = "Not authorized") : base(message)
        {
        }
    }
}
