﻿using AutoMapper;
using FFD.Auth.Application.CQRS.Authentication.Dto;
using FFD.Auth.Application.CQRS.User.Commands;
using FFD.Auth.Core.Entities.Dto;
using FFD.Auth.Core.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFD.Auth.Application.CQRS
{
    public class MappingProfile : Profile
    {

        public MappingProfile()
        {
            #region user
            CreateMap<RegisterUserCommand, Core.Entities.Db.User>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => Guid.NewGuid()))
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                .ForMember(dest => dest.Login, opt => opt.MapFrom(src => src.Login))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.Address))
                .ForMember(dest => dest.Role, opt => opt.MapFrom(src => "Client"))
                .ForMember(dest => dest.ConfirmEmail, opt => opt.MapFrom(src => false))                
                .ForMember(dest => dest.MiddleName, opt => opt.MapFrom(src => src.MiddleName));
            CreateMap<CreateUserCommand, Core.Entities.Db.User>()
                 .ForMember(dest => dest.Id, opt => opt.MapFrom(src => Guid.NewGuid()))
                 .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status))
                 .ForMember(dest => dest.Role, opt => opt.MapFrom(src => src.Role))
                 .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                 .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                 .ForMember(dest => dest.Login, opt => opt.MapFrom(src => src.Login))
                 .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))                
                 .ForMember(dest => dest.MiddleName, opt => opt.MapFrom(src => src.MiddleName))
                 .ForMember(dest => dest.RequiredChangePassword, opt => opt.MapFrom(src => true))
                 .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status))
                 .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.Address))
                 .ForMember(dest => dest.ConfirmEmail, opt => opt.MapFrom(src => false))
                 .ForMember(dest => dest.Avatar, opt => opt.MapFrom(src => src.Avatar));

            CreateMap<UpdateUserCommand, Core.Entities.Db.User>()
                .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
                .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
                .ForMember(dest => dest.Login, opt => opt.MapFrom(src => src.Login))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.MiddleName, opt => opt.MapFrom(src => src.MiddleName))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status))
                .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.Address))
                .ForMember(dest => dest.Role, opt => opt.MapFrom(src => src.Role))
                .ForMember(dest => dest.Avatar, opt => opt.MapFrom(src => src.Avatar));

            CreateMap<RefreshToken, Core.Entities.Db.User>()
               .ForMember(dest => dest.RefreshToken, opt => opt.MapFrom(src => src.Token))
               .ForMember(dest => dest.TokenCreated, opt => opt.MapFrom(src => src.Created))
               .ForMember(dest => dest.TokenExpires, opt => opt.MapFrom(src => src.Expires));
            CreateMap<Core.Entities.Db.User, UserInfoDto>();
            CreateMap<Core.Entities.Db.User, UserDto>()
             .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
             .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
             .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
             .ForMember(dest => dest.MiddleName, opt => opt.MapFrom(src => src.MiddleName))
             .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status))
             .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
             .ForMember(dest => dest.Avatar, opt => opt.MapFrom(src => src.Avatar))
             .ForMember(dest => dest.Address, opt => opt.MapFrom(src => src.Address))
             .ForMember(dest => dest.TokenCreated, opt => opt.MapFrom(src => src.TokenCreated))
             .ForMember(dest => dest.TokenExpires, opt => opt.MapFrom(src => src.TokenExpires));


            //CreateMap<Core.Entities.Db.User, UserDisplayDto>()
            //   .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            //   .ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FirstName))
            //   .ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LastName))
            //   .ForMember(dest => dest.MiddleName, opt => opt.MapFrom(src => src.MiddleName))
            //   .ForMember(dest => dest.Statud, opt => opt.MapFrom(src => src.Status))
            //   .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
            //   .ForMember(dest => dest.Picture, opt => opt.MapFrom(src => src.Picture))
            //   .ForMember(dest => dest.TokenCreated, opt => opt.MapFrom(src => src.TokenCreated))
            //   .ForMember(dest => dest.TokenExpires, opt => opt.MapFrom(src => src.TokenExpires));
            #endregion


        }

    }
}
