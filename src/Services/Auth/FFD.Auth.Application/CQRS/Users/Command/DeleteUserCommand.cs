﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;
using MediatR;
using FFD.Auth.Core;
using FFD.Auth.Application.Exceptions;

namespace FFD.Auth.Application.CQRS.User.Commands
{   
  public class DeleteUserCommand : IRequest<Unit>
    {
        public Guid Id { get; set; }
    }

    public class DeleteUserCommandHandler : IRequestHandler<DeleteUserCommand, Unit>
    {
        private readonly IDbContext _dbContext;

        public DeleteUserCommandHandler(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<Unit> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
        {
            var User = await _dbContext.Users.FirstOrDefaultAsync(i => i.Id == request.Id, cancellationToken);
            if (User == null)
            {
                throw new NotFoundException();
            }
            _dbContext.Users.Remove(User);
            await _dbContext.SaveChangesAsync();
            return Unit.Value;
        }
    }
}
