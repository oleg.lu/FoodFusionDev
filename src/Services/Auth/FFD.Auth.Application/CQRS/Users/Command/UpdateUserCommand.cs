﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;
using AutoMapper;
using FluentValidation;
using MediatR;
using FFD.Auth.Core;
using FFD.Auth.Core.IServices;
using FFD.Auth.Application.Exceptions;

namespace FFD.Auth.Application.CQRS.User.Commands
{   
    public class UpdateUserCommand : IRequest<Unit>
    {
        public Guid Id { get; set; }
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public string? MiddleName { get; set; }        
        public string? Login { get; set; }
        public string? Email { get; set; }
        public string? Password { get; set; } 
        public string? Status { get; set; }
        public string? Avatar { get; set; }
        public string? Role { get; set; }
        public string? Address { get; set; }
    }

    public class UpdateUserCommandHandler : IRequestHandler<UpdateUserCommand, Unit>
    {
        private readonly IDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly ITokenGenerator _tokenGenerator;

        public UpdateUserCommandHandler(IDbContext dbContext, IMapper mapper, ITokenGenerator tokenGenerator)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _tokenGenerator = tokenGenerator;
        }

        public async Task<Unit> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            var user = await _dbContext.Users.FirstOrDefaultAsync(i => i.Id == request.Id);
            if (user == null)
            {
                throw new NotFoundException();
            }
            _mapper.Map(request, user);
            if (!String.IsNullOrEmpty(request.Password))
            {
                _tokenGenerator.CreatePasswordHash(request.Password, out byte[] passwordHash, out byte[] passwordSalt);
                user.PasswordSalt = passwordSalt;
                user.PasswordHash = passwordHash;
            }
            await _dbContext.SaveChangesAsync(cancellationToken);
            return Unit.Value;
        }
    }

    public sealed class UpdateUserCommandValidator : AbstractValidator<UpdateUserCommand>
    {
        public UpdateUserCommandValidator()
        {
            RuleFor(x => x.Id).NotEmpty();
        }
    }
}
