﻿using FFD.Auth.Application.CQRS.Authentication;
using MediatR;
using FFD.Auth.Core.Entities.Db;
using FFD.Auth.Core;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using FFD.Auth.Application.CQRS.Authentication.Dto;
using FFD.Auth.Core.IServices;
using System.Security.Claims;
using FDD.Shared.Interfases;

namespace FFD.Auth.Application.CQRS.Authentication.Queries
{ 
    public class GetCurrentUserInfoQuery : IRequest<UserInfoDto>
    {
    }

    public class GetCurrentUserInfoQueryHandler : IRequestHandler<GetCurrentUserInfoQuery, UserInfoDto>
    {
        private readonly IUserAccessor _userAccessor;
        private readonly IDbContext _dbContext;
        private readonly IMapper _mapper;
        public GetCurrentUserInfoQueryHandler(IUserAccessor userAccessor, IDbContext dbContext, IMapper mapper)
        {
            _userAccessor = userAccessor;
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public Task<UserInfoDto> Handle(GetCurrentUserInfoQuery request, CancellationToken cancellationToken)
        {
            var userInfo = new UserInfoDto
            {
                Id = _userAccessor.Id.Value,
                Email = _userAccessor.User.FindFirstValue(ClaimTypes.Email),
                Role = _userAccessor.User.FindFirstValue("Role")
                //Roles = _userAccessor.User.FindAll(ClaimTypes.Role).Select(r => r.Value)    
            };
            var CurrentUser = _dbContext.Users.FirstOrDefault(x => x.Id == userInfo.Id);
            //.Include(x => x.Group.Roles)           
            return Task.FromResult(_mapper.Map<UserInfoDto>(CurrentUser));
        }
    }
}
