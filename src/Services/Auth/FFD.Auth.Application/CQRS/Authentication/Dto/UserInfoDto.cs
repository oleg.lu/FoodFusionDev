﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFD.Auth.Application.CQRS.Authentication.Dto
{
    public class UserInfoDto
    {
        public Guid Id { get; set; }

        public string Email { get; set; }
        public string DisplayNameRu { get; set; }
        public string DisplayNameEn { get; set; }
        public string Login { get; set; }
        public string? Avatar { get; set; }       
        public string Role { get; set; }
    }
}
