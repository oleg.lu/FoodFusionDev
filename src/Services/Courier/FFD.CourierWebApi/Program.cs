using FFD.Courier.Api;
using FFD.Courier.Api.Listener;
using FFD.Courier.Application;
using FFD.Courier.Infrastructure;
using FFD.Shared.Extensions;
using FFD.Shared.Models.Settings;
using FFD.Shared.RabbitMq.Settings;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDbContext<ApplicationDbContext>(options =>
                                                       options.UseNpgsql(builder.Configuration.GetConnectionString("DefaultConnection")));

//auth
var authenticationSection = builder.Configuration.GetSection("Authentication");
var authOptions = authenticationSection.Get<AuthOptions>();
builder.Services.Configure<AuthOptions>(authenticationSection);
var jwtOptions = JwtService.GetJwtBearerOptions(authOptions);
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
.AddJwtBearer(options =>
{
    options.RequireHttpsMetadata = jwtOptions.RequireHttpsMetadata;
    options.SaveToken = jwtOptions.SaveToken;
    options.TokenValidationParameters = jwtOptions.TokenValidationParameters;
});
// Add services to the container.
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(c =>
{
    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        In = ParameterLocation.Header,
        Description = "Please insert JWT with Bearer into field",
        Name = "Authorization",
        Type = SecuritySchemeType.ApiKey,
    });
    c.AddSecurityRequirement(new OpenApiSecurityRequirement
        {
            {
                new OpenApiSecurityScheme
                {
                   Reference = new OpenApiReference
                   {
                     Type = ReferenceType.SecurityScheme,
                     Id = "Bearer"
                   }
                },
                new string[] { }
            }
        });
});

builder.Services.AddHttpContextAccessor();
//Подключаем MediatR
builder.Services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblies(Assembly.GetExecutingAssembly()));
builder.Services.AddMediatR(cfg => cfg.RegisterServicesFromAssembly(typeof(Application).Assembly));

builder.Services.AddAutoMapper(typeof(Application));
builder.InjectDependencies();

//rabbit config
var brokerSettings = new BrokerSettings();
builder.Configuration.GetSection("RabbitMQ").GetSection("Broker").Bind(brokerSettings);
builder.Services.AddSingleton(brokerSettings);

var receiverSettings = new ReceiverSettings();
builder.Configuration.GetSection("RabbitMQ").GetSection("Listener").Bind(receiverSettings);
builder.Services.AddSingleton(receiverSettings);

//builder.Services.AddHostedService<CourierQueueListener>();
builder.Services.AddHostedService<CourierQueueListener>(_ => new CourierQueueListener(brokerSettings, receiverSettings, _.GetRequiredService<IServiceScopeFactory>()));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseRouting();
app.UseAuthorization();

app.MapControllers();

app.Run();
