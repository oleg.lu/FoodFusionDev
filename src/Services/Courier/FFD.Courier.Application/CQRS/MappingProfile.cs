﻿using AutoMapper;
using FFD.Courier.Core.Entities.Db;
using FFD.Courier.Core.Entities.Dto;
namespace FFD.Courier.Application.CQRS
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Core.Entities.Db.Chat, MessageDto>();
            CreateMap<Order, OrderDto>();           
        }
    }
}
