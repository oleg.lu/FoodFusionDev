﻿using AutoMapper;
using FDD.Shared.Interfases;
using FFD.Courier.Core;
using FFD.Courier.Core.Entities.Db;
using FFD.Courier.Core.Entities.Dto;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFD.Courier.Application.CQRS.Chat.Command
{
    public class GetMyOrdersCommand : IRequest<IEnumerable<OrderDto>>
    {     
    }
    public class GetMyOrdersCommandHandler : IRequestHandler<GetMyOrdersCommand, IEnumerable<OrderDto>>
    {
        private readonly IDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IUserAccessor _userAccessor;
        public GetMyOrdersCommandHandler(IDbContext dbContext, IMapper mapper, IUserAccessor userAccessor)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _userAccessor = userAccessor;
        }
        public async Task<IEnumerable<OrderDto>> Handle(GetMyOrdersCommand request, CancellationToken cancellationToken)
        {
            var orders = _dbContext.Orders.Where(x => x.CourierId == _userAccessor.Id && x.Status !="Finished");           
            var result = _mapper.Map<IEnumerable<OrderDto>>(orders);
            return result;         
        }
    }
    public sealed class GetMyOrdersCommandValidator : AbstractValidator<GetMyOrdersCommand>
    {
    }
}
