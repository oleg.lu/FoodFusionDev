﻿using AutoMapper;
using FDD.Shared.Interfases;
using FFD.Courier.Core;
using FFD.Courier.Core.Entities.Db;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFD.Courier.Application.CQRS.Chat.Command
{
    public class AssignOrderToMeCommand : IRequest<bool>
    {
        public int? OrderId { get; set; }      
    }
    public class AssignOrderToMeCommandHandler : IRequestHandler<AssignOrderToMeCommand, bool>
    {
        private readonly IDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IUserAccessor _userAccessor;
        public AssignOrderToMeCommandHandler(IDbContext dbContext, IMapper mapper, IUserAccessor userAccessor)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _userAccessor = userAccessor;
        }

        public async Task<bool> Handle(AssignOrderToMeCommand request, CancellationToken cancellationToken)
        {
            Order? order = _dbContext.Orders.FirstOrDefault(x => x.Id == request.OrderId);
            if (order == null)
            {
                throw new ArgumentException();
            }

            order.CourierId = _userAccessor.Id;
            order.Status = "Delivery";
            await _dbContext.SaveChangesAsync();
            return false;
        }
    }
    public sealed class AssignOrderToMeCommandValidator : AbstractValidator<AssignOrderToMeCommand>
    {
    }
}
