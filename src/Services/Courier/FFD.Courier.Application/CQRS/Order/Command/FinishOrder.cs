﻿using AutoMapper;
using FDD.Shared.Interfases;
using FFD.Courier.Core;
using FFD.Courier.Core.Entities.Db;
using FluentValidation;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFD.Courier.Application.CQRS.Chat.Command
{
    public class FinishOrderCommand : IRequest<bool>
    {
        public int? OrderId { get; set; }      
    }
    public class FinishOrderCommandCommandHandler : IRequestHandler<FinishOrderCommand, bool>
    {
        private readonly IDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IUserAccessor _userAccessor;
        public FinishOrderCommandCommandHandler(IDbContext dbContext, IMapper mapper, IUserAccessor userAccessor)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _userAccessor = userAccessor;
        }

        public async Task<bool> Handle(FinishOrderCommand request, CancellationToken cancellationToken)
        {
            try
            {
                Order? order = _dbContext.Orders.FirstOrDefault(x => x.Id == request.OrderId);
                if (order == null)
                {
                     throw new ("order not found") ;
                }
                order.Status = "Finished";
                await _dbContext.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
    public sealed class FinishOrderCommandValidator : AbstractValidator<AssignOrderToMeCommand>
    {
    }
}
