﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFD.Courier.Core.Entities.Dto
{
    public class OrderDto
    {
        public int Id { get; set; }

        public int? Weight { get; set; }

        public Guid? CourierId { get; set; }

        public string Status { get; set; }
        public string Address { get; set; }
        public string? User { get; set; }      
        public Guid? CookId { get; set; }
        
        public string Dishes { get; set; }
    }
}
