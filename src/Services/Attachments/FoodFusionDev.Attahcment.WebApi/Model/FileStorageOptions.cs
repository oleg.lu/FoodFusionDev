﻿namespace FoodFusionDev.Attahcment.WebApi.Model
{
    public class FileStorageOptions
    {
        public string Endpoint { get; set; }
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }
    }
}
