﻿using FoodFusionDev.Attahcment.WebApi.Model;
using FoodFusionDev.Attahcment.WebApi.Service;
using Microsoft.AspNetCore.Mvc;
using Minio;
using Minio.DataModel.Args;

namespace FoodFusionDev.Attahcment.WebApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AttachmentController : ControllerBase
    {      
        private readonly ILogger<AttachmentController> _logger;
        private readonly IMinioClient _minioClient;

        public AttachmentController(ILogger<AttachmentController> logger, IMinioClient minioClient)
        {
            _logger = logger;
            _minioClient = minioClient;
        }

        [HttpGet]
        public async Task<ActionResult> Get(string objectname, UploadTypeList bucket)
        {

            var result = await _minioClient.GetObjectAsync(bucket.ToString(), objectname);

            return File(result.data, result.objectstat.ContentType);
        }

        [HttpPost]
        public async Task<ActionResult> Post(UploadRequest request)
        {

            var result = await _minioClient.PutObjectAsync(new PutObjectRequest()
            {
                bucket = request.type.ToString(),
                data = request.data

            });

            return Ok(new { filename = result });
        }


    }
}
