using Microsoft.AspNetCore.Authentication.OAuth;
using Microsoft.OpenApi.Models;
using System.Text.Json.Serialization;
using System.Text;
using System.Reflection;
using FoodFusionDev.Attahcment.WebApi.Model;
using Minio;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
var optionsSection = builder.Configuration.GetSection("FileStorageConfig");
var option = optionsSection.Get<FileStorageOptions>();

builder.Services.AddMinio(option.AccessKey, option.SecretKey);
builder.Services.AddMinio(configureClient => configureClient
           .WithEndpoint(option.Endpoint)
           .WithCredentials(option.AccessKey, option.SecretKey));


builder.Services.AddControllers();

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
