#See https://aka.ms/customizecontainer to learn how to customize your debug container and how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:8.0 AS base
USER app
WORKDIR /app
EXPOSE 8080
EXPOSE 8081

FROM mcr.microsoft.com/dotnet/sdk:8.0 AS build
ARG BUILD_CONFIGURATION=Release
WORKDIR /src
COPY ["Shared/FFD.Shared.csproj", "Shared/"]
COPY ["Courier/FFD.CourierWebApi/FFD.Courier.Api.csproj", "Courier/FFD.CourierWebApi/"]
COPY ["Courier/FFD.Courier.Application/FFD.Courier.Application.csproj", "Courier/FFD.Courier.Application/"]
COPY ["Courier/FFD.Courier.Core/FFD.Courier.Core.csproj", "Courier/FFD.Courier.Core/"]
RUN dotnet restore "./Courier/FFD.CourierWebApi/./FFD.Courier.Api.csproj"
COPY . .
WORKDIR "/src/Courier/FFD.CourierWebApi"
RUN dotnet build "./FFD.Courier.Api.csproj" -c $BUILD_CONFIGURATION -o /app/build

FROM build AS publish
ARG BUILD_CONFIGURATION=Release
RUN dotnet publish "./FFD.Courier.Api.csproj" -c $BUILD_CONFIGURATION -o /app/publish /p:UseAppHost=false

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "FFD.Courier.Api.dll"]

