interface DishImage {
    id: string;
    shortName: string;
    imageUrl: string;
  };