interface DishDataType {
  id: number;
  name: string;
  price: number;
  description: string;
  quantity: number;
  summ: number;
}

