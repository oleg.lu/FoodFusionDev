import React, { useState } from 'react';
import axios, { AxiosRequestConfig, AxiosResponse, Method } from 'axios';
import { $realApi } from '@/services/api';
import { IResponseModel } from '@/interfaces/common';

// interface Success<T> {
//     status: 'success';
//     serverStatus: 200 | 300;
//     data: T;
// }

// interface Fault {
//     status: 'fault';
//     serverStatus: 400 | 500;
//     data: string;
// }

// interface ServerResult<T> {
//     result: Success<T> | Fault;
// };

// const resultData: ServerResult<Function> = {
//     result: {
//         status: 'fault',
//         serverStatus: 400,
//         data: '{}'
//     }
// }

// if (resultData.result.status === 'success') {
//     resultData.result.data
// } else if (resultData.result.status === 'fault') {
//     resultData.result.data
// }

interface UseApiHook<T> {
    data: T | null;
    error: any;
    loading: boolean;
    success: boolean;
    fetchData: (data?: any) => Promise<void>;
}

export const useApi = <T>(url: string, method: Method = 'GET'): UseApiHook<T> => {
    // const url = url;
    const [data, setData] = useState<T | null>(null);
    const [error, setError] = useState<any>(null);
    const [success, setSuccess] = useState<boolean>(false);
    const [loading, setLoading] = useState<boolean>(false);
    const fetchData = async (data?: any) => {
        setSuccess(false);
        setError(null);
        setLoading(true);
        try {
            const response: AxiosResponse<IResponseModel<T>> = await $realApi({ url, method, data });
            setData(response.data.body);
            setSuccess(response.data.result == 200);
            setLoading(false);
            // return response.data.body
        } catch (error) {
            setError(error);
            setSuccess(false);
            setLoading(false);
            // return error
        }
    };
    const fetcher = { fetchData, data, error, success, loading };
    return fetcher;
    // return { data, error, success, loading, fetchData };
};
