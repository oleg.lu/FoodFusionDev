import React, { useState } from 'react';
import { LaptopOutlined, MenuFoldOutlined, MenuUnfoldOutlined, NotificationOutlined, SearchOutlined, ShoppingCartOutlined } from '@ant-design/icons';
import type { MenuProps } from 'antd';
import { Avatar, Badge, Breadcrumb, Button, Input, Layout, Menu, theme } from 'antd';
import Brand from '../Brand';
// import Header1 from '../Header';
import { useSelector } from 'react-redux';
import { getCurrentUser, getUserAuth } from '@/features/selectors/currentUser';
// import localStyles from "./Users.module.scss";
import Anonim from '../../images/Anonim.png';
// import { avatar } from '@/pages/Users/styled';
// import './avatarStyle.css';
// import { LayoutsProps } from '@/interfaces/common';
// import { renderMenuItems } from '@/Config';
import { Outlet, Route } from 'react-router-dom';
import { CartBlock } from '../Menu/cart-menu/cart-block';
const { Header, Content, Sider } = Layout;
import MenuGridPage from '../../pages/MenuGrid';
import KitchenPage from '../../pages/Kitchen';
import CourierPage from '../../pages/Courier';
import ChatPage from '../../pages/Chat';
// const items1: MenuProps['items'] = ['1', '2', '3'].map((key) => ({
//     key,
//     label: `nav ${key}`,
// }));

// const items2: MenuProps['items'] = [UserOutlined, LaptopOutlined, NotificationOutlined].map(
//     (icon, index) => {
//         const key = String(index + 1);

//         return {
//             key: `sub${key}`,
//             icon: React.createElement(icon),
//             label: `subnav ${key}`,
//             children: new Array(4).fill(null).map((_, j) => {
//                 const subKey = index * 4 + j + 1;
//                 return {
//                     key: subKey,
//                     label: `option${subKey}`,
//                 };
//             }),
//         };
//     },
// );

const Frame = (props: any) => {
    const { menuItems } = props;
    const isAuth = useSelector(getUserAuth);
    const currentUser = useSelector(getCurrentUser);
    const [message, setMessage] = useState<number>(1);
    const [collapsed, setCollapsed] = useState(false);
    const { token: { colorBgContainer, borderRadiusLG } } = theme.useToken();

    const toggleCollapsed = () => {
        setCollapsed(!collapsed);
    };
    return (
        <div>         
            {currentUser.role == 'Admin' &&
                (<Layout>
                    <Header style={{ display: 'flex', alignItems: 'left', backgroundColor: "white", paddingLeft: 0, height: "46px", background: "#1675e0" }}>
                        <Button size='large' type="primary" onClick={toggleCollapsed} style={{ marginBottom: 16, height: "100%", background: "#1675e0" }}>
                            {collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
                        </Button>
                        <div style={{ width: "25%" }}> </div>
                        <Input prefix={<SearchOutlined />} placeholder="Search" style={{ width: "25%", marginLeft: 'auto', margin: 10 }} /> 
                        <div style={{ marginLeft: "30%" }}>
                            <CartBlock></CartBlock>
                        </div>

                        <div style={{ marginLeft: 'auto', display: 'flex' }}>
                            <Badge count={message} offset={[0, 30]}>
                                <Avatar className='ant-avatar' src={currentUser.avatar == 'undefined' || currentUser.avatar == null ? Anonim : `${currentUser.avatar}`} /> 
                            </Badge>
                        </div>
                    </Header>
                    <Layout>
                        <Sider width={200} style={{ background: colorBgContainer }}
                            collapsed={collapsed}>
                            <Menu
                                mode="inline"
                                defaultSelectedKeys={['1']}
                                defaultOpenKeys={['Settings', 'Recycle']}
                                style={{ height: '100%', borderRight: 0 }}
                                items={menuItems}
                            />
                        </Sider>
                        <Layout style={{ padding: '0 24px 24px' }}>
                            <Content
                                style={{
                                    padding: 24,
                                    margin: 0,
                                    minHeight: 280,
                                    background: colorBgContainer,
                                    borderRadius: borderRadiusLG,
                                }}
                            >                               
                                <Outlet />
                            </Content>
                        </Layout>
                    </Layout>
                </Layout>)}
            {currentUser.role.toLowerCase() == 'cook' && <div>
                {/* Компонент для отображения повара */}
                <KitchenPage />
            </div>}
            {currentUser.role.toLowerCase() == 'client' && <div>
                {/* Компонент для отображения клиента */}
                <MenuGridPage />
            </div>}
            {currentUser.role.toLowerCase() == 'courier' && <div> 
            {/* Компонент для отображения курьера */}
             <CourierPage></CourierPage>
            {/* <Route path="/Chat/:chatId" element={<div><ChatPage/></div>} /> */}
            </div>}
        </div>);
};
export default Frame;