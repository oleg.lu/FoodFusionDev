import React, { useEffect, useState } from 'react';
import { ShoppingCartOutlined } from '@ant-design/icons';
import { useSelector } from 'react-redux';
import { CartMenu } from './cart-menu';
import { calcTotalPrice } from './utils';
import axios from 'axios';
import { Badge } from 'antd';

export const CartBlock = () => {
    const [isCartMenuVisible, setIsCartMenuVisible] = useState(false);
    
    const items: DishDataType[] = useSelector((state: any) => state.cart.itemsInCart)

    // const [items, setItems] = useState([]);
    // useEffect(() => {
    //     console.log("Монтируем cart-block");
        
    //     axios.get('/api/menubasket/Basket')
    //         .then((response) => {
    //             const dishes: DishDataType[] = response.data;
    //             setItems(dishes);
    //             console.log(dishes);
    //         }).catch((e) => console.error(e));
    // }, []);

    const totalPrice = calcTotalPrice(items);
    return (
        <Badge count={items.length} size="small">
        <div style={{ display: 'flex', fontSize: 25 }}>
            {/* <div>{items.length}</div> */}           
            <ShoppingCartOutlined onClick={() => setIsCartMenuVisible(!isCartMenuVisible)} className={`shop-cart-button ${isCartMenuVisible && 'active'}`} />
            {/* {totalPrice > 0 ? <div style={{ fontSize: 20, marginLeft: 20 }}>{totalPrice} руб.</div> : null} */}
            {/* Если true Тогда показываем */}
            {isCartMenuVisible && <CartMenu items={items}></CartMenu>}
        </div>
        </Badge>
    )
}