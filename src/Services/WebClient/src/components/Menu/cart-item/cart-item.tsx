import React from "react"
import './cart-item.css'

type CartItemProps = {
    title: string;
    price:number;
    count:number;
}
export const CartItem: React.FC<CartItemProps> = ({title,price,count}) => {
    return (
        <div className="cart-item">
            <span>{title}</span>
            <span>x{count}</span>
            <div className="cart-item__price">
                <span>{price} руб</span>
            </div>
        </div>
    )
}