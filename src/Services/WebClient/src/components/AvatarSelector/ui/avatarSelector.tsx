import { styled } from 'styled-components';
import { useEffect, useState } from 'react';
// import { useTranslation } from 'react-i18next';
import { Avatar, Button, Modal } from 'antd';
import { ExclamationCircleOutlined, UserOutlined } from '@ant-design/icons';
import { AvatarSelectorProps } from '../model/types/avatarSelectorTypes';
// import { Button, ButtonTheme } from '@/shared/ui/Button/Button';
import React from 'react';

const SelectorWrapper = styled.div`
    display: flex;
    flex-direction: column;
    align-self: center;
    align-items: center;
    row-gap: 20px;
    margin: 20px;
`;

// const ButtonWLabel = styled<{ warn: string }>(Button)`
//     justify-content: center;
//     cursor: pointer;
//     max-width: min-content;
//     position: relative;
//     & > * {
//         cursor: pointer;
//     }
//     ${(props) => {
//         const { theme: { colors }, warn } = props;

//         if (warn !== false) {
//             return (`
//             &:hover::after {
//             display: block;
//             content: "${warn}";
//             position: absolute;         
//             z-index: 2;           
//             border: 1px solid #d9d9d9;
//             padding: 10px 20px;
//             border-radius: 5px;
//             box-shadow: 3px 2px 5px grey;
//             top: calc(100% + 10px);
//             left: 50%;
//             transform: translateX(-50%);
//         `)
//         // background: ${colors.white};
//         // color: ${colors.black};
//         }
//     }}
// `;

export const AvatarSelector = (props: AvatarSelectorProps) => {
    const { userAvatar, onChange } = props;
    const [avatar, setAvatar] = useState<string | undefined>();
    // const { t } = useTranslation();

    const convertBase64 = (file: Blob) => {
        return new Promise((resolve, reject) => {
            const fileReader = new FileReader();
            const fileMaxSize = 1572864; // 1.5MB
            if (file.size > fileMaxSize) {
                Modal.error({
                    title: 'errorLabel',
                    icon: <ExclamationCircleOutlined />,
                    content: 'avatarSizeErrorLabel',
                    footer: null,
                    closable: true,
                });
                reject();
            }
            fileReader.readAsDataURL(file);
            fileReader.onload = () => {
                resolve(fileReader.result);
            };
            fileReader.onerror = (error) => {
                reject(error);
            };
        });
    };

    useEffect(() => {
        !!onChange && onChange(avatar);
    }, [avatar]);

    const handleOnChange = async (file?: Blob) => {
        if (file) {
            const newImage = (await convertBase64(file)) as string;
            setAvatar(newImage);
        }
    };

    const handleClick = (event: MouseEvent) => {
        if (avatar || userAvatar) {
            event.preventDefault();
            !!onChange && onChange(undefined);
            setAvatar(undefined);
        }
    };

    return (
        <div>
        <SelectorWrapper>
            <Avatar
                size={100}
                icon={<UserOutlined />}
                src={(avatar && `data:image/png;base64${avatar}`) || userAvatar}
            />
            <input
            style={{display:'none'}}
                accept="image/*"
                hidden
                id="avatar-image-upload"
                type="file"
                onChange={(e) => handleOnChange(e.target?.files?.[0])}
            />
            {!!onChange && (
                <Button
                    // themeType={ButtonTheme.PRIMARY}
                    onClick={(e:any)=> handleClick(e)}
                    // warn={!avatar && !userAvatar && 'avatarSizeErrorLabel'}
                >
                    <label htmlFor="avatar-image-upload">
                        { (avatar || userAvatar)
                                ? 'Delete avatar'
                                : 'Load avatar'
                        }
                    </label>
                </Button>
            )}
        </SelectorWrapper>
        </div>
    );
};
