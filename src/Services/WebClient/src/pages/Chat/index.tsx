import React, { useEffect, useState, useRef } from 'react';
import { HubConnectionBuilder } from '@microsoft/signalr';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import moment from 'moment';
import { USER_LOCALSTORAGE_TOKEN } from '@/constants';

const ChatComponent = () => {
    const { chatId } = useParams();
    const [connection, setConnection] = useState(null);
    const [messages, setMessages] = useState([]);
    const [message, setMessage] = useState('');
    const [error, setError] = useState(null);
    const latestMessage = useRef(null);
    const [sendMessageEnabled, setSendMessageEnabled] = useState(false);

    useEffect(() => {
        loadHistoryMessage();
        connect();
    }, []);

    const loadHistoryMessage = () => {
        axios.post('/api/Courier/Courier/GetMessages', { orderId: chatId })
            .then(response => {
                const errorMessage = response.data.result > 200;
                if (errorMessage)
                    setError("У вас не хватает прав на данный чат");
                else {
                    const data = response.data.body.map((item: any) => {
                        var date = moment(item.dataRecord).format('DD.MM.YYYY HH:mm');
                        if (item.clientId) return `${date}: Клиент(${item.client}): ${item.message}`;
                        if (item.courierId) return `${date}: Курьер(${item.courier || ''}): ${item.message}`;

                    });
                    setMessages(data);
                }

            })
            .catch(error => {
                console.error('Error loading queue records', error);
            });
    };

    const connect = () => {
        const connect = new HubConnectionBuilder()
            .withUrl("http://localhost:8099/chat", { accessTokenFactory: () => `${localStorage.getItem(USER_LOCALSTORAGE_TOKEN,)}` })
            .withAutomaticReconnect()
            .build();

        connect.on("ReciveMessage", (user, message) => {
            var date = moment().format('DD.MM.YYYY HH:mm');
            setMessages(oldMessages => [...oldMessages,

            `${date}:${user}: ${message}`]);
            latestMessage.current.scrollIntoView({ behavior: 'smooth' });
        });
        setSendMessageEnabled(true)
        connect.start().catch(err =>
            setSendMessageEnabled(false)
            // console.error(err.toString())            
        );
        setConnection(connect);

        return () => {
            connect.stop();
        };
    };

    useEffect(() => {
        if (connection?.state === "Connected") joinChat();
    }, [connection, chatId]);

    const joinChat = () => {
        connection.invoke("JoinChat", chatId);
    };

    const sendMessage = () => {
        if (connection) {
            joinChat();
            connection.invoke("Send", { message: message, chatId: chatId });
            setMessage('');
        }
    };

    const renderError = () => {
        return (
            <div className="error-container">
                <h2>Ошибка</h2>
                <p>{error}</p>
            </div>
        );
    };
    const getMessageStyle = (message: any) => {
        let backgroundColor = 'transparent'; // По умолчанию цвет фона - прозрачный

        // Проверяем содержимое сообщения и устанавливаем цвет фона
        if (message.includes(': Клиент(')) {
            backgroundColor = 'aquamarine'; // Аквамариновый цвет для Клиента
        } else if (message.includes(': Курьер(')) {
            backgroundColor = 'lightblue'; // Другой цвет для Курьера
        }
        return { backgroundColor };
    };
    return (
        <div className="chat-container">
            {error ? (
                renderError()
            ) : (
                <div>
                    <h2 className="chat-header">Чат по заказу: {chatId}</h2>
                    <div className="chat-messages">
                        {
                            messages.map((message, index) => (
                                <p key={index} className="chat-message" style={getMessageStyle(message)}>
                                    <div>{message}</div>
                                </p>
                            ))
                        }
                        <div ref={latestMessage} />
                    </div>
                    {sendMessageEnabled && <div className="chat-input-container">
                        <input
                            type="text"
                            value={message}
                            onChange={e => setMessage(e.target.value)}
                            placeholder="Enter your message"
                            className="chat-input"
                        />
                        <button onClick={sendMessage} className="chat-send-button">Send</button>
                    </div>
                    }
                    {!sendMessageEnabled && <div>Сервис отправки на данный момент не работает</div>}


                </div>
            )}
        </div>
    )
};

export default ChatComponent;