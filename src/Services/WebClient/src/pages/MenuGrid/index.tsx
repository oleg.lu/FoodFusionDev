import React, { useState, useEffect } from 'react';
import { Row, Col, Input, Select, Button, Modal, Radio, Avatar, Badge, message } from 'antd';
import axios from 'axios';
import MenuItem from '@/components/Menu/menu-item';
import Background from '../../images/background2.jpg';
import { Header, Stack } from 'rsuite';
import { CloseCircleOutlined, MenuFoldOutlined, MenuUnfoldOutlined, SearchOutlined } from '@ant-design/icons';
import { CartBlock } from '@/components/Menu/cart-menu/cart-block';
import { useSelector } from 'react-redux';
import { getCurrentUser } from '@/features/selectors/currentUser';
import Anonim from '../../images/Anonim.png';
// import CloseOutlined from '@ant-design/icons-svg/lib/asn/CloseOutlined';

const { Search } = Input;
const { Option } = Select;

function Page() {
    const [cards, setCards] = useState([]);
    const [imageObjs, setImagesObjs] = useState([]);
    const [filteredCards, setFilteredCards] = useState([]);
    const [searchText, setSearchText] = useState('');
    const [selectedCategory, setSelectedCategory] = useState('');
    const currentUser = useSelector(getCurrentUser);
    useEffect(() => {
        // Получение данных через axios
        axios.get('/api/menubasket/Menu')
            .then(response => {
                setCards(response.data);  // Установка данных в состояние
            });
        axios.get('/api/menubasket/DishImage')
            .then(response => {
                setImagesObjs(response.data);  // Установка данных в состояние
            });
        console.log('Монтриуем MenuGrid Page')
        // axios.post('/api/menubasket/Basket', null)
        //     .then((response) => { console.log(response.data) }).catch((e) => console.error(e))
    }, []);  // Пустая зависимость, чтобы получить данные только при монтировании компонента

    const getRandomImage = () => {
        const randomIndex = Math.floor(Math.random() * imageObjs.length);
        const randomDishObj: DishImage = (imageObjs)[randomIndex];
        const { imageUrl } = randomDishObj;
        return imageUrl
    };
    const backgroundImageStyle = {
        backgroundImage: `url(${Background})`,
        // backgroundSize: 'cover',
        backgroundPosition: 'center',
        // backgroundttachment: 'fixed',
        width: '100vw',
        height: '100vh',
        overflow: 'overlay'
    };
    //
    // Функция фильтрации записей по тексту и категории
    const filterCards = () => {
        let filtered = cards.filter(card => {
            // Фильтрация по тексту
            const nameIncludesText = card.name.toLowerCase().includes(searchText.toLowerCase());
            // Фильтрация по категории (если выбрана)
            const categoryMatches = selectedCategory ? card.category === selectedCategory : true;
            return nameIncludesText && categoryMatches;
        });
        setFilteredCards(filtered);
    };

    // Обработчик изменения текста поиска
    const handleSearch = (value: any) => {
        setSearchText(value);
    };

    // Обработчик изменения выбранной категории
    const handleCategoryChange = (value: any) => {
        setSelectedCategory(value);
    };

    // Вызываем функцию фильтрации при изменении текста поиска или выборе категории
    useEffect(() => {
        filterCards();
    }, [searchText, selectedCategory, cards]);
    //
    const clearCategory = () => {
        setSelectedCategory('');
    };

    const [isModalVisible, setIsModalVisible] = useState(false);
    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };
    return (
        <div style={backgroundImageStyle}>
            <Header style={{ display: 'flex', alignItems: 'left', backgroundColor: "white", paddingLeft: 0, height: "46px", background: "#1675e0", paddingTop: '7px' }}>
                {/* <div className="logo" /> */}
                {/* <Brand /> */}
                {/* <Button size='large' type="primary" onClick={toggleCollapsed} style={{ marginBottom: 16, height: "100%", background: "#1675e0" }}>
                    {collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
                </Button> */}
                <div style={{ width: "15%" }}></div>
                <div style={{ width: "25%" }}>
                    {/* <Input prefix={<SearchOutlined />} placeholder="Search" style={{ width: "25%", marginLeft: 'auto', margin: 10 }} /> Встроенная поисковая строка */}
                    <Search placeholder="Введите название блюда" onSearch={handleSearch} enterButton />
                </div>
                <div style={{ width: "30%" }}>
                    <Radio.Group onChange={(e) => { handleCategoryChange(e.target.value); handleCancel(); }} value={selectedCategory} style={{ borderRadius: '8px', padding: '4px', boxShadow: '0 2px 4px rgba(0,0,0,0.1)' }}>
                        <Radio style={{ color: 'black' }} value="">Все категории</Radio>
                        <Radio style={{ color: 'black' }} value="Бургеры">Бургеры</Radio>
                        <Radio style={{ color: 'black' }} value="Горячие блюда">Горячие блюда</Radio>
                        <Radio style={{ color: 'black' }} value="Закуски">Закуски</Radio>
                        <Radio style={{ color: 'black' }} value="Пицца">Пицца</Radio>
                    </Radio.Group>
                </div>
                <div style={{ marginLeft: "15%", display: "-webkit-inline-box" }}>
                    <CartBlock></CartBlock>
                </div>

                <div style={{ marginLeft: 'auto', display: 'flex' }}>
                    {/* <Badge count={message} offset={[0, 30]}> */}
                        <Avatar className='ant-avatar' src={currentUser.avatar == 'undefined' || currentUser.avatar == null ? Anonim : `${currentUser.avatar}`} /> {/* Замените ссылку на реальный URL вашей аватарки пользователя */}
                    {/* </Badge> */}
                </div>
                <div style={{ marginLeft: 'auto', display: 'flex', width: "1%"}}></div>
            </Header>
            <Row justify="center" style={{ padding: '20px 0' }}>
                <Col span={8}>

                </Col>
                <Col span={24} ></Col>
                <Col span={8} >

                </Col>
            </Row>
            <Row gutter={[16, 16]} justify="center">
                {filteredCards.map((card, index) => (
                    <Col key={index} xs={24} sm={12} md={8} lg={6}>
                        <MenuItem item={card} imageUrl={getRandomImage()} />
                    </Col>
                ))}
            </Row>
        </div>
    );
}

export default Page;
