import React, { useState, useEffect } from 'react';
import { Button, Avatar, Table, TabsProps, Tabs } from 'antd';
import axios from 'axios';
import { useSelector } from 'react-redux';
import { getCurrentUser } from '@/features/selectors/currentUser';
import { Header } from 'rsuite';
import Anonim from '../../images/Anonim.png';
import { Link } from 'react-router-dom';

function Page() {
    const [queueRecords, setQueueRecords] = useState([]);
    const [myRecords, setMyRecords] = useState([]);
    const [allRecords, setAllRecords] = useState([]);
    const currentUser = useSelector(getCurrentUser);

    useEffect(() => {
        loadLists();
    }, []);

    const loadLists = () => {
        loadQueueRecords();
        loadMyRecords();
        loadAllRecords();
    };

    const loadQueueRecords = () => {
        axios.get('/api/Courier/Courier/GetOrders')
            .then(response => {
                setQueueRecords(response.data.body);
            })
            .catch(error => {
                console.error('Error loading queue records', error);
            });
    };

    const loadMyRecords = () => {
        axios.get('/api/Courier/Courier/GetMyOrders', {})
            .then(response => {
                setMyRecords(response.data.body);
            })
            .catch(error => {
                console.error('Error loading my records', error);
            });
    };

    const loadAllRecords = () => {
        axios.get('/api/Courier/Courier/GetFinishOrders')
            .then(response => {
                setAllRecords(response.data.body);
            })
            .catch(error => {
                console.error('Error loading all records', error);
            });
    };

    const handleButtonClick = (id: number) => {
        axios.post('api/Courier/Courier/AssignOrderToMe', { OrderId: id })
            .then(response => {
                console.log('Request sent successfully', response);
                loadLists(); // Reload lists after action
            })
            .catch(error => {
                console.error('Error sending request', error);
            });
    };

    const ButtonFinishClick = (id: number) => {
        axios.post('api/Courier/Courier/FinishOrder', { OrderId: id })
            .then(response => {
                console.log('Request sent successfully', response);
                loadLists(); // Reload lists after action
            })
            .catch(error => {
                console.error('Error sending request', error);
            });
    };

    const myColumns = [
        { title: 'ID', dataIndex: 'id', key: 'id' },
        {
            title: 'User', dataIndex: 'user', key: 'user', render: (text: any) => {
                if (text) {
                    const user = JSON.parse(text);
                    return `${user.FirstName} ${user.LastName}`;
                }
                return "";
            }
        },
        { title: 'Status', dataIndex: 'status', key: 'status' },
        {
            title: 'Action',
            key: 'action',
            render: (text: any, record: any) => (
                <Button onClick={() => ButtonFinishClick(record.id)} type="primary" >
                    Завершить
                </Button>
            ),
        },
        {
            title: '',
            key: 'action',
            render: (text: any, record: any) => (
                record.courierId != null &&
            //      <Button onClick={() => handleButtonClick(record.id)} type="primary" >
            //      Чат
            //  </Button>
            <Link to={`/chat/${record.id}`} >
            <button disabled={!record.id}>Чат</button>

          </Link>
            ),
        },
    ];

    const queueColumns = [
        { title: 'ID', dataIndex: 'id', key: 'id' },
        {
            title: 'User', dataIndex: 'user', key: 'user', render: (text: any) => {
                if (text) {
                    const user = JSON.parse(text);
                    return `${user.FirstName} ${user.LastName}`;
                }
                return "";
            }
        },
        { title: 'Status', dataIndex: 'status', key: 'status' },
        {
            title: 'Action',
            key: 'action',
            render: (text: any, record: any) => (
                record.status == 'Cooked' &&
                <Button onClick={() => handleButtonClick(record.id)} type="primary" >
                    Взять в работу
                </Button>
            ),
        },
        // {
        //     title: '',
        //     key: 'action',
        //     render: (text: any, record: any) => (
        //         record.status == 'Cooked' &&
        //     //      <Button onClick={() => handleButtonClick(record.id)} type="primary" >
        //     //      Чат
        //     //  </Button>
        //     <Link to={`/chat/${record.id}`} >
        //     <button disabled={!record.id}>Чат</button>

        //   </Link>
        //     ),
        // },
    ];

    const finishColumns = [
        { title: 'ID', dataIndex: 'id', key: 'id' },
        {
            title: 'User', dataIndex: 'user', key: 'user', render: (text: any) => {
                if (text) {
                    const user = JSON.parse(text);
                    return `${user.FirstName} ${user.LastName}`;
                }
                return "";
            }
        },
        { title: 'Status', dataIndex: 'status', key: 'status' },
        {
            title: '',
            key: 'action',
            render: (text: any, record: any) => (
                record.courierId != null &&
            //      <Button onClick={() => handleButtonClick(record.id)} type="primary" >
            //      Чат
            //  </Button>
            <Link to={`/chat/${record.id}`} >
            <button disabled={!record.id}>Чат</button>

          </Link>
            ),
        },
    ];

    const expandedRowRenderQueue = (record: any) => {
        const dishes = JSON.parse(record.dishes);
        const columns = [
            { title: 'Dish Name', dataIndex: 'dishName', key: 'dishName' },
            { title: 'Dish Quantity', dataIndex: 'dishQuantity', key: 'dishQuantity' },
        ];
        return <Table columns={columns} dataSource={dishes} pagination={false} rowKey="dishId" />;
    };

    const expandedRowRenderMy = (record: any) => {
        const dishes = JSON.parse(record.dishes);
        const columns = [
            { title: 'Dish Name', dataIndex: 'dishName', key: 'dishName' },
            { title: 'Dish Quantity', dataIndex: 'dishQuantity', key: 'dishQuantity' },
        ];
        return <Table columns={columns} dataSource={dishes} pagination={false} rowKey="dishId" />;
    };

    const expandedRowRenderAll = (record: any) => {
        const dishes = JSON.parse(record.dishes);
        const columns = [
            { title: 'Dish Name', dataIndex: 'dishName', key: 'dishName' },
            { title: 'Dish Quantity', dataIndex: 'dishQuantity', key: 'dishQuantity' },
        ];
        return <Table columns={columns} dataSource={dishes} pagination={false} rowKey="dishId" />;
    };

    const items = [
        {
            key: '1',
            label: 'Очередь',
            children:
                <div>
                    <Table dataSource={queueRecords} columns={queueColumns} expandable={{ expandedRowRender: expandedRowRenderQueue }} rowKey="id" />
                </div>,
        },
        {
            key: '2',
            label: 'Мои заказы',
            children: <div>
                <Table dataSource={myRecords} columns={myColumns} expandable={{ expandedRowRender: expandedRowRenderMy }} rowKey="id" />
            </div>,
        },
        {
            key: '3',
            label: 'Завершенные заказы',
            children: <div>
                <Table dataSource={allRecords} columns={finishColumns} expandable={{ expandedRowRender: expandedRowRenderAll }} rowKey="id" />
            </div>,
        },
    ];

    return (
        <div>
            <Header style={{ display: 'flex', alignItems: 'center', backgroundColor: "#4CAF50", paddingLeft: 20, height: 60 }}>
                <h3 style={{ color: 'white', marginRight: 'auto' }}>Сервис доставки</h3>
                <Avatar className='ant-avatar' src={currentUser.avatar === 'undefined' || currentUser.avatar === null ? Anonim : `${currentUser.avatar}`} />
            </Header>
            <Tabs defaultActiveKey="1" items={items} />;
        </div>
    );
}

export default Page;
