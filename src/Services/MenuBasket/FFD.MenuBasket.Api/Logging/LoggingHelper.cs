﻿using FFD.MenuBasket.Api.Controllers;
using FFD.MenuBasket.DataAccess.Repositories;
using FFD.MenuBasket.Domain.Entities;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Model;
using System;

namespace FFD.MenuBasket.Api.Logging
{
    public static class LoggingHelper
    {
        //FOR CONTROLLERS
        public static void LogGetMethod<T>(ILogger<T> logger, IEnumerable<BaseEntity> entityCollection)
        {
            logger.LogInformation($"Get all method from {typeof(T)} result :");
            foreach (var e in entityCollection)
            {
                logger.LogInformation($"ID {e.Id} NAME {e.ToString()}");
            }
        }
        public static void LogGetMethod<T>(ILogger<T> logger, BaseEntity entity)
        {
            logger.LogInformation($"Get entity by ID method from {typeof(T)} result :");
            logger.LogInformation($"ID {entity.Id} NAME {entity.ToString()}");
        }

        public static void LogPostMethod<T, E>(ILogger<T> logger, BaseEntity entity, int id)
        {
            logger.LogCritical($"Post {typeof(E)} (Name-{entity.ToString()} Id-{id}) to DB");
        }

        public static void LogControllerPostException<T>(ILogger<T> logger, Exception e)
        {
            logger.LogError($"Post call :\n{e.Message}");
        }

        //MIDDLEWARE
        public static void LogMiddlewareShare<T>(ILogger<T> logger)
        {
            logger.LogError("An unhandled exception occurred during request processing");
        }
        public static void LogMiddlewareValidation<T>(ILogger<T> logger, string errors)
        {
            logger.LogError($"Validation exception occurred: {errors}");
        }
        public static void LogMiddlewareNotFound<T>(ILogger<T> logger, string errors)
        {
            logger.LogError($"Not Found exception occurred: {errors}");
        }
    }
}
