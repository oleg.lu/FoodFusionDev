
using FDD.Shared.Interfases;
using FFD.MenuBasket.Api.Middleware;
using FFD.MenuBasket.DataAccess.EntityFramework;
using FFD.MenuBasket.DataAccess.Repositories;
using FFD.MenuBasket.Domain.Entities;
using FFD.MenuBasketDataAccess.Repositories;
using FFD.OrderService.WebHost.Notifier;
using FFD.Shared.Extensions;
using FFD.Shared.Models.Settings;
using FFD.Shared.RabbitMq;
using FFD.Shared.RabbitMq.Settings;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System.Text.Json.Serialization;
using System.Xml.Linq;
using FFD.MenuBasket.DataAccess.Mongo;
using FDD.Shared.Services;
using Serilog;

namespace FFD.MenuBasket.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            const string Origin = "Rules00"; //for CORS
            var builder = WebApplication.CreateBuilder(args);

            builder.Services.AddCors(options =>
            {
                options.AddPolicy(name: Origin, corsBuilder =>
                {
                    corsBuilder.WithOrigins(builder.Configuration.GetSection("CORS:Origins").Get<string[]>())
                    .WithHeaders(builder.Configuration.GetSection("CORS:Headers").Get<string[]>())
                    .WithMethods(builder.Configuration.GetSection("CORS:Methods").Get<string[]>());
                });
            }); //CORS

            var authenticationSection = builder.Configuration.GetSection("Authentication");
            var authOptions = authenticationSection.Get<AuthOptions>();
            builder.Services.Configure<AuthOptions>(authenticationSection);
            var jwtOptions = JwtService.GetJwtBearerOptions(authOptions);
            builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.RequireHttpsMetadata = jwtOptions.RequireHttpsMetadata;
                options.SaveToken = jwtOptions.SaveToken;
                options.TokenValidationParameters = jwtOptions.TokenValidationParameters;
            });

            builder.Services.AddControllers();
            //builder.Services.AddAuthentication(Configuration);
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(c =>
            {
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    In = ParameterLocation.Header,
                    Description = "Please insert JWT with Bearer into field",
                    Name = "Authorization",
                    Type = SecuritySchemeType.ApiKey
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
        {
            {
                new OpenApiSecurityScheme
                {
                   Reference = new OpenApiReference
                   {
                     Type = ReferenceType.SecurityScheme,
                     Id = "Bearer"
                   }
                },
                new string[] { }
            }
        });
            });

            //postgres
            DbContextOptionsBuilder<MyDatabaseContext> optionsBuilder = new DbContextOptionsBuilder<MyDatabaseContext>();
            DbContextOptions<MyDatabaseContext> options = optionsBuilder.UseNpgsql(builder.Configuration.GetConnectionString("Remote")).Options;
            builder.Services.AddSingleton(typeof(DbContext), new MyDatabaseContext(options));
            builder.Services.AddSingleton<IRepository<Dish>, EfRepository<Dish>>();
            builder.Services.AddSingleton<IRepository<Basket>, EfRepository<Basket>>();
            builder.Services.AddScoped<IUserAccessor, UserAccessor>();

            #region RabbitMq configure
            builder.Services.Configure<SenderSettings>(builder.Configuration.GetSection("RabbitMq").GetSection("Sender"));
            builder.Services.Configure<BrokerSettings>(builder.Configuration.GetSection("RabbitMq").GetSection("Broker"));
            builder.Services.AddScoped<QueueSender>(provider =>
            {
                var senderSettings = provider.GetRequiredService<IOptions<SenderSettings>>().Value;
                var brokerSettings = provider.GetRequiredService<IOptions<BrokerSettings>>().Value;
                return new QueueSender(senderSettings, brokerSettings);
            });

            builder.Services.AddScoped<Notifier>();
            builder.Services.AddHttpContextAccessor();

            //var senderSection = builder.Configuration.GetSection("RabbitMq").GetSection("Sender");
            //var brokerSection = builder.Configuration.GetSection("RabbitMq").GetSection("Broker");
            //bool.TryParse(senderSection["Durable"], out var durable);
            //var queueSettings = new SenderSettings
            //{
            //    Queue = senderSection["Queue"],
            //    Durable = durable,
            //    ExchangeType = senderSection["ExchangeType"],
            //    Exchange = senderSection["Exchange"]
            //};
            //var brokerSettings = new BrokerSettings
            //{
            //    Host = brokerSection["Host"],
            //    Port = int.Parse(brokerSection["Port"]),
            //    User = brokerSection["User"],
            //    Password = brokerSection["Password"]
            //};
            //builder.Services.AddScoped((provider) =>
            //{
            //    return new QueueSender(senderSection, brokerSection);
            //});
            #endregion

            #region MongoDb configure
            builder.Services.AddScoped<IMongoService, MongoService>();
            //Seed
            var client = new MongoClient(builder.Configuration.GetConnectionString("MongoRemote"));
            var database = client.GetDatabase("ffd_dish_images_db");
            //DB
            builder.Services.AddScoped<IMongoDatabase>(provider => database);
            MongoDataBaseSeeder.DbSeed(database);

            //builder.Services.AddScoped<IMongoDatabase>(provider =>
            //{
            //    var client = new MongoClient(builder.Configuration.GetConnectionString("MongoLocal"));
            //    return client.GetDatabase("ffd_dish_images_db");

            //});
            #endregion


            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.Console()
                .WriteTo.File(@"C:\Logs123\ffd -.log", rollingInterval: RollingInterval.Day)
                .CreateLogger();

            //������� ��������� �����
            builder.Services.AddControllers().AddJsonOptions(x =>
                x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);
            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseMiddleware<MyExceptionHandlerMiddleware>();
            app.UseCors(Origin);
            app.UseHttpsRedirection();

            app.UseAuthorization();

            app.MapControllers();

            app.Run();
        }
    }
}