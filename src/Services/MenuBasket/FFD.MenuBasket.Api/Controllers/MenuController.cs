﻿using FFD.MenuBasket.Api.Logging;
using FFD.MenuBasket.DataAccess.Repositories;
using FFD.MenuBasket.Domain.Entities;
using FFD.MenuBasket.Domain.SubModels;
using Microsoft.AspNetCore.Mvc;

namespace FFD.MenuBasket.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MenuController : ControllerBase
    {
        private readonly IRepository<Dish> _menuRepo;

        private readonly ILogger<MenuController> _logger;

        public MenuController(IRepository<Dish> menuPero, ILogger<MenuController> logger)
        {
            _menuRepo = menuPero;
            _logger = logger;
        }
        /// <summary>
        /// Добавление новго Dish в БД
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> AddNewDishAsync(CreateOrEditDishRequest request)
        {
            Dish dish = new()
            {
                Name = request.Name,
                Price = request.Price,
                Description = request.Description,
                Category = request.Category,
            };
            try
            {
                var id = await _menuRepo.AddNewDishAsync(dish);
                LoggingHelper.LogPostMethod<MenuController, Dish>(_logger, dish, id);
                return Ok(id);
            }
            catch (Exception e)
            {
                LoggingHelper.LogControllerPostException(_logger,e);
                throw;
            }
            
        }
        /// <summary>
        /// Получить Dish по ID
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<Dish>> GetByIdAsync(int id)
        {
            var dish = await _menuRepo.GetByIdAsync(id);
            LoggingHelper.LogGetMethod(_logger, dish);
            return Ok(dish);
        }
        /// <summary>
        /// Получить список всех Dishes
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Dish>>> GetAllAsync()
        {
            var dishes = await _menuRepo.GetAllAsync();

            LoggingHelper.LogGetMethod(_logger, dishes);

            return Ok(dishes);
        }
        /// <summary>
        /// Удаление Dish по ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteDish(int id)
        {
            bool isSuccess = await _menuRepo.DeleteDishByIdAsync(id);

            if (!isSuccess)
                return NotFound(id);
            return Ok($"Dish с id = {id} удален.");
        }

        /// <summary>
        /// Изменить Dish по ID
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<ActionResult> UdateByIdAsync(int id, CreateOrEditDishRequest request)
        {
            bool isSuccess = await _menuRepo.DeleteDishByIdAsync(id);

            if (!isSuccess)
                return NotFound(id);

            Dish dish = new()
            {
                Id = id,
                Name = request.Name,
                Price = request.Price,
                Description = request.Description,
                Category = request.Category,
            };
            var i = await _menuRepo.AddNewDishAsync(dish);
            return Ok(i);
        }

    }
}
