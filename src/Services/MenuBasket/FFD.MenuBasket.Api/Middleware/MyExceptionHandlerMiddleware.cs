﻿using FFD.MenuBasket.Api.Logging;
using FFD.MenuBasket.Domain.Exceptions;
using FluentValidation;
using System.Net;
using System.Text.Json;

namespace FFD.MenuBasket.Api.Middleware
{
    public class MyExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<MyExceptionHandlerMiddleware> _logger;

        public MyExceptionHandlerMiddleware(RequestDelegate next, ILogger<MyExceptionHandlerMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }


        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception exception)
            {
                LoggingHelper.LogMiddlewareShare(_logger);
                await HandleExceptionAsync(context, exception);
            }
        }

        private Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var code = HttpStatusCode.InternalServerError;
            var result = string.Empty;
            switch (exception)
            {
                case ValidationException validationException:
                    code = HttpStatusCode.BadRequest;
                    result = JsonSerializer.Serialize(validationException.Errors);
                    LoggingHelper.LogMiddlewareNotFound(_logger, result);
                    break;
                case NotFoundException:
                    code = HttpStatusCode.NotFound;
                    LoggingHelper.LogMiddlewareNotFound(_logger,exception.Message);
                    break;
            }
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;

            if (result == string.Empty)
            {
                result = JsonSerializer.Serialize(new { error = exception.Message });
            }

            return context.Response.WriteAsync(result);
        }
    }
}