﻿using FFD.MenuBasket.DataAccess.Data;
using FFD.MenuBasket.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace FFD.MenuBasket.DataAccess.EntityFramework
{
    public class MyDatabaseContext : DbContext
    {

        public DbSet<Dish> Dishes { get; set; }
        public DbSet<Basket> Baskets { get; set; }
        public DbSet<DishBasket> DishBaskets { get; set; }
        public DbSet<Order> Orders { get; set; }
        public MyDatabaseContext(DbContextOptions<MyDatabaseContext> options) : base(options)
        {
            Database.EnsureDeleted();
            Database.EnsureCreated();
            Seed();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<DishBasket>().HasKey(sc => new { sc.DishId, sc.BasketId });
            modelBuilder.Entity<DishBasket>()
                .HasOne(sc => sc.Dish)
                .WithMany(s => s.DishBasket)
                .HasForeignKey(sc => sc.DishId);
            modelBuilder.Entity<DishBasket>()
                .HasOne(sc => sc.Basket)
                .WithMany(s => s.DishBasket)
                .HasForeignKey(sc => sc.BasketId);
        }
        private void Seed()
        {
            Dishes.AddRange(FakeDataFactory.Dishes);
            SaveChanges();
        }

    }
}
