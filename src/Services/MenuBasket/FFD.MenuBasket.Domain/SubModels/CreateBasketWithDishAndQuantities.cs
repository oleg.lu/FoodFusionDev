﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFD.MenuBasket.Domain.SubModels
{
    public class CreateBasketWithDishAndQuantities
    {
        public int DishId { get; set; }
        public int DishQuantity { get; set; }

    }
}
