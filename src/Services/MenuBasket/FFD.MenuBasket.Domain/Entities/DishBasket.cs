﻿namespace FFD.MenuBasket.Domain.Entities
{
    public class DishBasket
    {
        public int DishId { get; set; }
        public Dish? Dish { get; set; }
        public int DishQuantity { get; set; }

        public int BasketId { get; set; }
        public Basket? Basket { get; set; }
    }
}


