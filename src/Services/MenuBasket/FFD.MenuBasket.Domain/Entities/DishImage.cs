﻿
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace FFD.MenuBasket.Domain.Entities
{
    public class DishImage
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement("short_name")]
        public string ShortName { get; set; }
        [BsonElement("image_data")]
        public string ImageUrl { get; set; }
    }
}
