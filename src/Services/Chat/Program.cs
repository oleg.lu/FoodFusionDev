using FDD.Shared.Interfases;
using FDD.Shared.Services;
using FFD.Chat.DBL;
using FFD.Chat.Hubs;
using FFD.Chat.Services;
using FFD.Shared.Extensions;
using FFD.Shared.Models.Settings;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddDbContext<ApplicationDbContext>(options =>
                                                       options.UseNpgsql(builder.Configuration.GetConnectionString("DefaultConnection")));
//auth
var authenticationSection = builder.Configuration.GetSection("Authentication");
var authOptions = authenticationSection.Get<AuthOptions>();
builder.Services.Configure<AuthOptions>(authenticationSection);
var jwtOptions = JwtService.GetJwtBearerOptions(authOptions);
builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
.AddJwtBearer(options =>
{
    options.RequireHttpsMetadata = jwtOptions.RequireHttpsMetadata;
    options.SaveToken = jwtOptions.SaveToken;
    options.TokenValidationParameters = jwtOptions.TokenValidationParameters;
    options.Events = new JwtBearerEvents
    {
        OnMessageReceived = context =>
        {
            var accessToken = context.Request.Query["access_token"];

            // ���� ������ ��������� ����
            var path = context.HttpContext.Request.Path;
            if (!string.IsNullOrEmpty(accessToken) &&
                (path.StartsWithSegments("/chat")))
            {
                // �������� ����� �� ������ �������
                context.Token = accessToken;
            }
            return Task.CompletedTask;
        }
    };
});

builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(policy =>
    {
        policy//.AllowAnyOrigin()
        .WithOrigins("http://localhost:3000")
        .AllowAnyHeader()
        .AllowAnyMethod()
        .AllowCredentials();
    });
});

builder.Services.AddScoped<IDbContext, ApplicationDbContext>();
//builder.Services.AddScoped<IUserAccessor, UserAccessorSignalR>();
builder.Services.AddHttpContextAccessor();
builder.Services.AddSignalR();
var app = builder.Build();
app.UseCors();
app.MapHub<ChatHub>("/chat", options => {
    //options.ApplicationMaxBufferSize = 10000;
    //options.TransportMaxBufferSize = 64;
    //options.LongPolling.PollTimeout = System.TimeSpan.FromMinutes(1);
    options.Transports = HttpTransportType.WebSockets;
});
app.UseAuthentication();
app.UseAuthorization();

app.Run();
