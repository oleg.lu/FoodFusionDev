﻿using System.Text.Json;
using System.Text.RegularExpressions;
using FDD.Shared.Interfases;
using FFD.Chat.DBL;
using FFD.Chat.Services;
using FFD.Shared.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
namespace FFD.Chat.Hubs
{
    [Authorize]
    public class ChatHub:Hub<IChatClient>
    {
        private IDbContext _context;
        //private readonly IUserAccessor _userAccessor;
        public ChatHub(IDbContext context)
        {
            _context = context;
            //_userAccessor = userAccessor;
        }
        public async Task JoinChat(string chatId)
        {
            var user = new UserAccessorSignalR(Context.User);
            //var chat = await _context.Chats.FindAsync(chatId);
            //if (chat != null)
            {
                await Groups.AddToGroupAsync(Context.ConnectionId, chatId);
                await this.Clients.Group(chatId)
                .ReciveMessage("Admin", $"{user.FirstName} {user.LastName} в чате");
                //chatId.ToString());
            }
        }
        public async Task Send(Message message)
        {
            string responseRole = "";
            var user = new UserAccessorSignalR(Context.User);
            var order = await _context.Orders.FirstOrDefaultAsync(x => x.Id.ToString() == message.chatId && (x.ClientId == user.Id || x.CourierId == user.Id || user.Role == "Admin"));
            if (order != null)
            {
                //var stringConnection = await _cache.GetAsync(Context.ConnectionId);
                //var connection = JsonSerializer.Deserialize<UserConnection>(stringConnection);               
                var client = JsonSerializer.Deserialize<UserDto>(order.User);
                var chatToSave = new DBL.Model.ChatModel()
                {
                    //ClientId = client.Id,
                    //CourierId = order.CourierId,
                    Message = message.message,
                    DataRecord = DateTime.Now,
                    OrderId = order.Id,
                    Attachment = message.attachment != null ? message.attachment : null,
                };
                if (user.Role == "Client")
                {
                    chatToSave.ClientId = user.Id;
                    responseRole = "Клиент";
                }
                if (user.Role == "Courier")
                {
                    chatToSave.CourierId = user.Id;
                    responseRole = "Курьер";
                }
                _context.Chats.Add(chatToSave);
                await _context.SaveChangesAsync();

                await Clients.Group(message.chatId).ReciveMessage($@"{responseRole}", $@"{message.message}");
            }
            else
                Clients.Group(message.chatId).ReciveMessage("Receive", $@"Такого заказа не существует. Не удалось добавить сообщение {message} для чата {message.chatId}");
            //await this.Clients.Group(message.chatId).ReciveMessage("Admin", $"{user.FirstName} {user.LastName} в чате");
            //await this.Clients.All.SendAsync("Receive", message, $@"tt1");
        }
        //public override async Task OnConnectedAsync()
        //{
        //    await this.Clients.All.SendAsync("Notify", $"{Context.ConnectionId} вошел в чат");
        //    await base.OnConnectedAsync();
        //}
        //public override async Task OnDisconnectedAsync(Exception? exception)
        //{
        //    await Clients.All.SendAsync("Notify", $"{Context.ConnectionId} покинул в чат");
        //    await base.OnDisconnectedAsync(exception);
        //}
    }
    public record UserConnection (string? UserName, string ChatRoom);
    public record Message(string? message, string chatId, string? attachment = null);
    public interface IChatClient
    {
        public Task ReciveMessage(string UserName, string message);
        public Task SendMesage(string message, int chatId);
    }
}
