﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFD.Shared.Models.Settings
{
    public class AuthOptions
    {
        public JwtTokenOptions JwtToken { get; set; }
    }

    public class JwtTokenOptions
    {
        public string Issuer { get; set; } = "Tempalte"; // издатель токена
        public string Audience { get; set; } = "Audience"; // потребитель токена
        public string SecretKey { get; set; } = "super!secretkey@";   // ключ для шифрации
        public int Lifetime { get; set; } = 600;
    }
}
