﻿namespace FDD.Bus.Models
{
    public class BusCreateOrder
    {
        ///<example>75a6fd5d-83bc-4658-8b78-9e280d8327f8</example>
        public Guid ClientId { get; set; }
        public DateTime RegistrationDate { get; set; }

        /// <example>
        /// [  { "dishId": "d47f7656-180e-4bda-bf3f-f63f1d1b839b",  "price": 123,  "discount": 4,  "count": 3 },
        ///    { "dishId": "e98a270a-22c1-49dd-b010-09fbbdcb69b7",  "price": 67.59,  "discount": 14,  "count": 2 },
        ///    { "dishId": "bbc58ea8-dbb4-431d-afe0-225f4ca8e118",  "price": 560.42,  "discount": 250,  "count": 4 },
        ///    { "dishId": "dd221c6b-3592-40b4-8e6c-dab0014fc1f9",  "price": 98.19,  "discount": 101,  "count": 21 }
        /// ]
        /// </example>
        public List<BusCreateOrderItem> Items { get; set; } = [];
    }
}