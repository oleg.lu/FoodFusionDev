﻿namespace FDD.Bus.Models
{
    public class BusCreateOrderItem
    {
        public Guid DishId { get; set; }
        public uint Count { get; set; }
        public double Price { get; set; }
    }
}