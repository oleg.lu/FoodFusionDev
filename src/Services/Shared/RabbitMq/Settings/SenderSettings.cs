﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FFD.Shared.RabbitMq.Settings
{
    public class SenderSettings
    {
        public string Queue { get; set; }
        public bool Durable { get; set; }
        public string Exchange { get; set; }
        public string ExchangeType { get; set; }
    }
}
