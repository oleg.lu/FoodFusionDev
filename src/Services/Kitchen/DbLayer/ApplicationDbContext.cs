﻿using FDD.Shared.Interfases;
using FFD.KitchenApi.DbLayer.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace FFD.KitchenApi.DbLayer
{
    public class ApplicationDbContext : DbContext, IDbContext
    {
        //private readonly IUserAccessor _userAccessor;
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options, IUserAccessor userAccessor = null) : base(options)
        {
            AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
            //_userAccessor = userAccessor;
            //Database.EnsureDeleted();
            Database.EnsureCreated();
        }
        public DbSet<Order> Orders { get; set; }
        //public DbSet<Chat> Chats { get; set; }
    }
}
