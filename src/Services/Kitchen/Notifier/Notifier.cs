﻿using FDD.Shared.Interfases;
using FFD.KitchenApi.DbLayer.Model;
using FFD.Shared.Models;
using FFD.Shared.RabbitMq;
using System.Diagnostics;
using System.Text.Json;


namespace FFD.KitchenApi.Notifie
{
    public class Notifier
    {
        private readonly QueueSender _queueSender;
        private readonly IUserAccessor _userAccessor;
        
        public Notifier(QueueSender queueSender, IUserAccessor userAccessor)
        {
            _queueSender = queueSender;
            _userAccessor = userAccessor;
        }

        public void NotifyMessage(DbLayer.Model.Order order)
        {
            //var uesrId = _userAccessor.Id;
            //var message = JsonSerializer.Deserialize<DishRabbit>(data);
            //var message = new DishRabbit {
            //    ListDish= listDish,
            //    OrderId= orderId
            //};
            //message.User = new UserDto
            //{
            //    FirstName = _userAccessor.FirstName,
            //    LastName = _userAccessor.LastName,
            //    Id = _userAccessor.Id,
            //    MiddleName = _userAccessor.MiddleName,
            //    Login = _userAccessor.Login,
            //    Email= _userAccessor.Email               
            //};
            _queueSender.Send(JsonSerializer.Serialize(order), ["Courier"]);
            Debug.WriteLine(order);
        }

    }
}
