using FDD.Shared.Interfases;
using FFD.KitchenApi.DbLayer;
using FFD.KitchenApi.DbLayer.Model;
using FFD.KitchenApi.Notifie;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FFD.KitchenApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class KitchenController : ControllerBase
    {      
        private readonly ILogger<KitchenController> _logger;
        private IDbContext _dbContext;
        private IUserAccessor _userAccessor;
        private readonly Notifier _notifier;
        public KitchenController(ILogger<KitchenController> logger, IDbContext dbContext, IUserAccessor userAccessor, Notifier notifier)
        {
            _logger = logger;           
            _dbContext = dbContext;
            _userAccessor = userAccessor;
            _notifier = notifier;
        }

        [HttpPost("SetStatusCookedToOrder")]
        public async Task<ActionResult<object>> SetStatusCookedToOrder(long id, CancellationToken cancellationToken = default)
        {
            Order? order;
            try
            {
                order = await _dbContext.Orders.FirstOrDefaultAsync(o => o.Id == id);
                if (order != null)
                {
                    order.Status = "Cooked";
                    order.CookId = _userAccessor.Id;
                    await _dbContext.SaveChangesAsync();
                    _notifier.NotifyMessage(order);
                }
                else
                    return NotFound();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            return Ok(order);
        }
        [HttpGet("GetActualOrder")]
        public async Task<ActionResult<List<Order>>> GetActualOrder(CancellationToken cancellationToken = default)
        {
            try
            {
                var orders = _dbContext.Orders.Where(x => x.Status != "Cooked").ToList();
                return Ok(orders);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
            
        }
    }
}
